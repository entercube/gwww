<%--
프로그램 ID 	: GWW-1560G-J
프로그램기능 	: 접속점검 
작성일 		: 2021. 01. 01  
Version		: 0.9 
작성자		: 홍세준(nightfog777@gmail.com)
// https://logging.apache.org/log4j/2.x/download.html 
--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
<%
 
	// app시간시간 측정 및 변수설정 
	long stime =0, etime=0, diff=0; 
	
	stime = System.currentTimeMillis();
	

	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1. 
	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	response.setDateHeader("Expires", 0); 
 
	//------------------------------ Common Header Start --------------------------------------
 
	//------------------------------ Common Header End   --------------------------------------
	request.setCharacterEncoding("utf-8");	
 
 	String  sessingString = session.getId(); 
 	
	String	urlSchmeStr	  = request.getScheme() + "://" + request.getServerName();
	
	String  AgentString   = request.getHeader("User-Agent");
	 
	String  AgentUser      	= AgentString.toLowerCase();
 
  	// ip 확인 
   	String 	 ConnectedClientIP = "";  
 
	if ( request.getHeader("X-Forwarded-For") != null && request.getHeader("X-Forwarded-For").length() > 0  ) {
		// 로드밸런서 상의 IP 
		ConnectedClientIP = request.getHeader("X-Forwarded-For");
	}
	else {
		// 단독접속 IP 
		ConnectedClientIP = request.getRemoteAddr();
	}
%>


<html><head>

<title>Verify2 URL</title>
 
</head>
	<body>
	
	 	<ul>
	 	
	 		<li>
	 			Session : <%=sessingString%>
	 		</li>  
 
 	 	
	 		<li>
	 			AgentUser : <%=AgentUser%>
	 		</li> 
	 		
	 		<li>
	 			schemeServer : <%=urlSchmeStr%>
	 		</li>
 
	 		 
	 		<li>
	 			IP : <%=ConnectedClientIP%>
	 		</li>
 
	 		<li>
	 			currentTime : <%=System.currentTimeMillis()%>
	 		</li>

	 			 		
	 			 		
	 	</ul>
	 	
	</body>
	
</html>
 
<%
 	// F1000. 실행시간측정 
	 etime = System.currentTimeMillis();		
	 diff = etime - stime; 
 	 System.out.println("===== V1.1 Time etime====" + etime + " diff=" + diff + " / IP=" + ConnectedClientIP );  
%>

