<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.util.Date, java.text.SimpleDateFormat" %>

<%
	// 일자 객체 선언 
    Date  nowDate = new Date();

	// 단순일자 객체 
    SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
	
	// 오늘 날자 출력변수 
    String strdate = simpleDate.format(nowDate);
	
	// 접속자 ip주소 
    String ipaddress = request.getRemoteAddr();
	
%>

 