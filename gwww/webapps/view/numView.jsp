<%--
- ID : SWEDU-SVT-3050-JP
- 명칭 : numView.jsp 
- 기능 : 오늘날자보기 
- Version : 0.9  
- 작성자 : sw.tutor (tutor@swkr.com) 
- 작성일 : 2020.08.05 
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="swedu.exercise.CalcNum" %>


<%@include file="/view/todayEmbed.jsp"%>
 
<jsp:useBean id="CN" class="swedu.exercise.CalcNum"/>
 
<%
   
	
     
    // P10. 변수선언 
    String myName   = "홍길동"; 
    int tnum 		= 0;		// 목표숫자 1   ex) 20 
    int inum 		= 0;		// 입력숫자 2   ex) 3
    int times 		= 0; 		// 연산횟수   	 ex) 4 
    
	int outNumAdd		= 0;		// 결과값  	ex) 20 + (3*4) = 32 
	int outNumMinus		= 0;		// 결과값  	ex) 20 + (3*4) = 32 
	
    // P20. 각각의 변수에 인자값 받음 
    if	( request.getParameter("tnum") != null ) { 
    	tnum = Integer.parseInt( request.getParameter("tnum") ); 
	}
	else {
		tnum = 100; 
	}
	
    // P30. 결과값 계산
    
    // 방식1 : 내부객체 생성 
    // CalcNum CN = new CalcNum(); 
	
	// 방식2 : 선언된 빈즈 사용  
	
	// 삼중 더하기 연산값 가져오기 
	outNumAdd = CN.numAdd(tnum, inum, times);
	
	// 삼중 빼기 연산값 가져오기 
	outNumMinus = CN.numMinus(tnum, inum, times);
	 
	
%>


<html>
<head>
	<meta charset="utf-8">
	<title>숫자처리 </title> 
	
	<style>
		.smallClass {
			font-size:0.9em;
			color:blue; 
		}
	</style>
</head>

<body>
 
 <!--  1. 숫자더하기  -->
	<h2>숫자 삼중 더하기</h2>
 
	<ul>
		<li>목표입력 : <%=tnum%></li>
		<li>입력숫자 : <%=inum%></li>
		<li>수행횟수 : <%=times%></li>
		<li><b><u>최종결과</u></b> : <%=outNumAdd%></li>
	</ul>
	 
	 <hr>
	 
 <!--  2. 숫자빼기  -->
	<h2>숫자 삼중 빼기</h2>
 
	<ul>
		<li>목표입력 : <%=tnum%></li>
		<li>입력숫자 : <%=inum%></li>
		<li>수행횟수 : <%=times%></li>
		<li><b><u>최종결과</u></b> : <%=outNumMinus%></li>
	</ul>
	 
	 <hr>
	 <span style='smallClass'>
	 	Connected at <%=ipaddress%>
	 	<p>
	 	- 실행일자 : <%=strdate%>
	 	<p>
	 	- 실행자 : <%=myName%>
	 </span>
	 
	 
</body>
</html>
