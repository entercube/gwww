<%--
- ID : SWEDU-SVT-3055-JP
- 명칭 : todayworld.jsp 
- 기능 : 오늘날자보기 언어인자에 따른 국제날자 표기 
- Version : 0.9  
- 작성자 : sw.tutor (tutor@swkr.com) 
- 작성일 : 2020.08.05 
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page  import="java.util.Locale, java.util.Date"
		  import="java.text.DateFormat, java.text.SimpleDateFormat"
%>

<%!
	/* 
	  - 언어코드에 따른 일자표기
	  - 인자값으로 전달된 언어코드에 따라서 다른 출력값 전달 
	*/ 
	private String viewWorldToday(String paramLangCode, Locale paramLocale) {
	
		String outDate 	= null; 	 		// 출력날자 
		 	
	    Date  nowDate 	= new Date();		// 오늘날자 
	    	
		DateFormat theDate = null; // 날자포맷
 
	    try {
	    	
	    	// 언어코드가 en 혹은 fr로 왔을 경우에 입력로케일에 관계없이 영어, 불어로 날자 전환 표기 
	    	if ( paramLangCode.equals("en")) {
	    		theDate = DateFormat.getDateInstance(DateFormat.LONG, Locale.US);
	    	} 
			else if ( paramLangCode.equals("fr")) {
				theDate = DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE);
	    	}
	    	else {
	    		// 로케일에 해당하는 날자정보 
	    		theDate = DateFormat.getDateInstance(DateFormat.LONG);
	    	}
	 
	        // 날자객체를 문자열로 전환 
		    outDate =theDate.format(nowDate);
		    
	    }
	    catch ( Exception ex ) {
	    	outDate = "--"; 
	    	ex.printStackTrace(); 
	    }
  
		return outDate; 
    }
%>


<%

	// P10. 변수
	String langCode 	= ""; 	// 언어인자 
	String strdate		= "";   // 보여줄 날자 
 	String titleView 	= "";   // 보여줄 제목 
	String dateDesc 	= "";   // 날자 설명 

	Locale 		locale 		= request.getLocale();	
	
	
	// P10. 언어코드 인자 받기 
	if	( request.getParameter("lang") != null && request.getParameter("lang").length() > 0 ) { 
		// 언어코드 ko, ch, en, jp, fr 등을 인자로 받음 
		// ex) http://localhost:8080/swbase/view/todayworld.jsp?lang=fr
		langCode = request.getParameter("lang"); 
	}
	else {
		// 인자가 없는 경우로서 기본 언어코드는 영어로 설정 
		langCode = "en"; 
 	}
	
	// P20. 언어코드에 해당하는 로케일 조회후 날자정보 받음 
	strdate = viewWorldToday(langCode, locale); 
	
	// P30. 
    if ( langCode.equals("ko")) {
    	// 한국어일 경우에 한국식으로 콘텐츠 표기 
    	titleView = "오늘 날자 안내"; 
    }
    else if ( langCode.equals("fr")) {
    	titleView = "Guide des dates du jour"; 
    }
    else {
    	// 그외 경우는 미국식으로 콘텐츠 표기 
    	titleView = "World Date Information"; 
    }
    
    
%>
 
<html>
<head>
	<meta charset="utf-8">
	<title>TodayWorld</title> 
</head>
<body>

	<h2><%=titleView%></h2>
	
	<hr>
	
	<%=strdate%> 
	
	<p>
	
	<span style='color:blue;' >
	== 홍길동이 만든 날짜 보이기 == 
	</span>
	
</body>
</html>
