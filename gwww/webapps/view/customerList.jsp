<%--
- ID : SWEDU-SVT-3060-JP
- 명칭 : custmoerList.jsp 
- 기능 : 고객리스트 
- Version : 0.9  
- 작성자 : sw.tutor (tutor@swkr.com) 
- 작성일 : 2020.08.05 
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 
<%@page 
 		import="java.net.URLEncoder, java.util.Vector" 
 		import="org.apache.log4j.Logger" 
 		import="swedu.util.DBConnection"
 		import="swbiz.Customer,swbiz.CustomerDAO"
 %>
 
 
<jsp:useBean id="CST" class="swbiz.Customer"/>
<jsp:useBean id="CSD" class="swbiz.CustomerDAO"/>


<%
//------------------------------ Common Header Start --------------------------------------
	
	// 프로그램ID 변수설정
 
	String prgID = "SWEDU-SVT-3060-JP";
	Logger logger = Logger.getLogger(prgID);  // 로거 
	
	// 접속이력기록 
	//------------------------------ Common Header End   --------------------------------------

	// 한글인코딩 
	request.setCharacterEncoding("utf-8");	
 
	// ============= TS1. 공통 시간측정 시점 ===============
	long stime = 0, etime= 0,  diff = 0;
	
	stime = System.currentTimeMillis();	
 
	
	// P10. 성씨 인자 받기 
	
	
	String sungStr = null; 
	
	if	( request.getParameter("sung") != null && request.getParameter("sung").length() > 0 ) { 
		// ex) http://3.34.125.216:8080/swbase/view/custmerList.jsp?sung=박
		sungStr = request.getParameter("sung"); 
	}
	else {
		// 인자가 없는 경우로서 기본값은 "이"씨 
		sungStr = "최"; 
 	}
	
 
 	// F30. 데이터 호출 
	Vector<swbiz.CustomerDAO> 		LSV				=	null;
 
 	// 데이터 목록가져오기 
 	 LSV = CST.getRecordAll( sungStr );	 // 풀링아닌 일반접속 
 	 
	// 전체수량 
	int allCnt = LSV.size();
   
	// ============= TE1. 공통 시간측정 종점 ===============
	etime = System.currentTimeMillis();
	
	diff = etime - stime;
  
	logger.info("#### START customerList V0.96 ####  sungStr=" + sungStr + " / allcnt=" + allCnt + " / diff=" + diff);
 
%> 
 
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	
	<title>고객목록</title> 
 
</head>

<body>
 
 	<div id="mainContainer" > 
 	
		<h2><span style='color:blue'><%=sungStr%></span>씨성 고객목록 [인원=<%=allCnt%>]</h2>
	 
		<div class='tableWrap' > 
		
			<table class='listTable' >
 
	    		<% 
	    		
	    			String outTmp = ""; 
	    			String infoStr = ""; 
	    			
	    			for (int i = 0; i < allCnt ; i++ ) { 
	    				
	    				CSD = LSV.elementAt(i);
  
	    				// 정보문자열 
	    				infoStr = CSD.rcd + " | " + CSD.name + "  | " + CSD.mail; 
	    				
	    				// 출력문자열 조합 
	   					outTmp = "<tr><td>" + infoStr + "</td></tr>"; 
	    			
	   					// 데이터 출력 
	   					out.println(outTmp);
	    			}
	   			%> 
			 
			</table>
			
		</div>
	
	</div>
	<!--  EOF mainContainer  -->
	
	
</body>
</html>
