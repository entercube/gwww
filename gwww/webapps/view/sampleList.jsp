<%--
- ID : SWEDU-SVT-3050-JP
- 명칭 : sampleList.jsp 
- 기능 : 샘플랜던인명보기 
- Version : 0.9  
- 작성자 : sw.tutor (tutor@swkr.com) 
- 작성일 : 2020.08.05 
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 
 
<%@page 
 		import="java.net.URLEncoder, java.util.Vector" 
 		import="org.apache.log4j.Logger" 
 		import="swedu.util.DBConnection"
 		import="swbiz.Sample,swbiz.SampleDAO"
 %>
 
 
<jsp:useBean id="CST" class="swbiz.Sample"/>
<jsp:useBean id="CSD" class="swbiz.SampleDAO"/>


<%
//------------------------------ Common Header Start --------------------------------------
	
	// 프로그램ID 변수설정
 
	String prgID = "SWEDU-SVT-3055-JP";
	Logger logger = Logger.getLogger(prgID);  // 로거 
	
	// 접속이력기록 
	//------------------------------ Common Header End   --------------------------------------

	// 한글인코딩 
	request.setCharacterEncoding("utf-8");	
 
	// ============= TS1. 공통 시간측정 시점 ===============
	long stime = 0, etime= 0,  diff = 0;
	
	stime = System.currentTimeMillis();	
 
	
	// P10. 성씨 인자 받기 
	
	
	String sungStr = null; 
	
	if	( request.getParameter("sung") != null && request.getParameter("sung").length() > 0 ) { 
		// ex) http://3.34.125.216:8080/swbase/view/sampleList.jsp?sung=박
		sungStr = request.getParameter("sung"); 
	}
	else {
		// 인자가 없는 경우로서 기본값은 "이"씨 
		sungStr = "윤"; 
 	}
	
 
 	// F30. 데이터 호출 
	Vector<swbiz.SampleDAO> 		LSV				=	null;
 
 	// 데이터 목록가져오기 
	LSV = CST.getRecordAll( sungStr, DBConnection.LOCAL_CONNECT);  // 로컬접속 
	// LSV = CST.getRecordAll( sungStr, DBConnection.POOL_CONNECT);	 // 풀링접속 
	// 전체수량 
	int allCnt = LSV.size();
  
	
	// ============= TE1. 공통 시간측정 종점 ===============
	etime = System.currentTimeMillis();
	
	diff = etime - stime;
  
	logger.info("#### START sampleList V0.95 ####  sungStr=" + sungStr + " / allcnt=" + allCnt + " / diff=" + diff);
 
%> 
 
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	
	<title>샘플 인원목록</title> 
 
</head>

<body>
 
 	<div id="mainContainer" > 
 	
		<h2><span style='color:blue'><%=sungStr%></span>씨성 랜덤목록 [인원=<%=allCnt%>]</h2>
	 
		<div class='tableWrap' style="background:#eeeeee;border:1px solid #777777;" > 
		
			<table class='listTable' >
 
	    		<% 
	    		
	    			String outTmp = ""; 
	    			String infoStr = ""; 
	    			
	    			String linkString = ""; 
	    			
	    			for (int i = 0; i < allCnt ; i++ ) { 
	    				
	    				CSD = LSV.elementAt(i);
   
	    				linkString = "<a href='sampleView.jsp?rcd=" + CSD.rcd + "' target='subWindow' >" + CSD.name + "</a>"; 
	    				// 정보문자열 
	    				infoStr = CSD.rcd + " | " + linkString + "  | " + CSD.mail; 
	    				
	    				// 출력문자열 조합 
	   					outTmp = "<tr><td>" + infoStr + "</td></tr>"; 
	    			
	   					// 데이터 출력 
	   					out.println(outTmp);
	    			}
	   			%> 
			 
			</table>
			
		</div>
	
	</div>
	<!--  EOF mainContainer  -->
	
	
</body>
</html>
