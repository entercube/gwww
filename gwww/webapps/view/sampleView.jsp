<%--
- ID : SWEDU-SVT-3050-JP
- 명칭 : sampleView.jsp 
- 기능 : 샘플정보 상세보기 
- Version : 0.9  
- 작성자 : sw.tutor (tutor@swkr.com) 
- 작성일 : 2020.08.05 
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 
<%@page 
 		import="java.net.URLEncoder, java.util.Vector" 
 		import="org.apache.log4j.Logger" 
 		import="swedu.util.DBConnection"
 		import="swbiz.Sample,swbiz.SampleDAO"
 %>
 
 
<jsp:useBean id="CST" class="swbiz.Sample"/>
<jsp:useBean id="CSD" class="swbiz.SampleDAO"/>


<%
//------------------------------ Common Header Start --------------------------------------
	
	// 프로그램ID 변수설정
 
	String prgID = "SWEDU-SVT-3056-JP";
	Logger logger = Logger.getLogger(prgID);  // 로거 
	
	// 접속이력기록 
	//------------------------------ Common Header End   --------------------------------------

	// 한글인코딩 
	request.setCharacterEncoding("utf-8");	
 
	// ============= TS1. 공통 시간측정 시점 ===============
	long stime = 0, etime= 0,  diff = 0;
	
	stime = System.currentTimeMillis();	
 
	 
	// P10. 랜덤인명코드 받기 
	String rcdVal = "";
	
	if	( request.getParameter("rcd") != null && request.getParameter("rcd").length() > 0 ) { 
		// 언어코드 ko, ch, en, jp, fr 등을 인자로 받음 
		// ex) http://localhost:8080/swbase/team/T1/sampleView.jsp
		rcdVal = request.getParameter("rcd"); 
		
		logger.info("T0 rcdVal=" + rcdVal); 
	}
	else {
		logger.info("T1  No rcd!!!"); 
 
		return;
 	}
	
	// P11. 인원정보 상세정보보기를 위한 객체 호출 
	CSD = CST.getRecord(rcdVal, DBConnection.LOCAL_CONNECT); 
	
	if ( CSD == null ) { 
		logger.info("Check Rcd rcdVal=" + rcdVal); 
		
		System.out.println("NO RCD"); 
		
		// response.sendRedirect("/swbase/team/T1/"); 
		return;
 	}
	
 	logger.info("#### START sampleView V0.91 ####  rcd=" + rcdVal + " / name=" + CSD.name + " / diff=" + diff);
 	 
%> 
 
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	
	<title>샘플 인원정보 상세보기</title> 
 	
 	<style>
 	
 		.titleClass {
 			color:blue;
			display:inline-block;
			width:160px;
 		}
 		
 		.contentClass {
 			display:inline-block;
 			color:purple; 
 		}
 		
 		.listTop {
 			margin-top:15px; 
 		}
 		
 		.listTop li {
 			margin-top:10px;
 			line-height:180%'
 		}
 		
 	</style>
</head>

<body>
 
 	<div id="mainContainer" > 
 	
		<h2><span style='color:blue'>상세정보보기</span></h2>
	 
		<div id='viewDetail' >
		
			<ul class='listTop' >
				
				<li>
					 <span class='titleClass' > ■ 랜덤코드 </span> 
					 <span class='contentClass' ><%=CSD.rcd%></span>    
				</li>
 
				<li>
					 <span class='titleClass' > ■ 랜덤인원명칭 </span> 
					 <span class='contentClass' ><%=CSD.name%></span>    
				</li>
				
				<li>
					 <span class='titleClass' > ■ 랜덤인명이메일 </span> 
					 <span class='contentClass' ><%=CSD.mail%></span>    
				</li>
				
				
			</ul>
			
		</div>
	
	</div>
	<!--  EOF mainContainer  -->
	
	
</body>
</html>
