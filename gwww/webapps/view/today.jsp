<%--
- ID : SWEDU-SVT-3050-JP
- 명칭 : today.jsp 
- 기능 : 오늘날자보기 
- Version : 0.9  
- 작성자 : sw.tutor (tutor@swkr.com) 
- 작성일 : 2020.08.05 
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.util.*, java.text.*" %>
 

<%
    Date  nowDate = new Date();
    SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
    String strdate = simpleDate.format(nowDate);
    
    String ipaddress = "";
    
    String serverName = request.getServerName();
    
    /*
    내부 주석 
    
    
    프로그램명 : numView.jsp 
    입력인자 : numView.jsp?tnum=20&inum=3&times=4
    출력변수 : outNum=32 
    */ 
%>


<html>
<head>
	<meta charset="utf-8">
	<title>Today-JSP</title> 
</head>
<body>
	<h2>오늘 날자 안내 - JSP</h2>
	<hr>
	오늘은 <b><%=strdate%></b>일 입니다.
 	- 이것은 내가 만든 페이지!!! 
 	<p>
 	서버명 : <%=serverName%>
</body>
</html>
