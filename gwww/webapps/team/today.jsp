<%--
- ID : SWEDU-SVT-3050-JP
- 명칭 : today.jsp 
- 기능 : 오늘날자보기 
- Version : 0.9  
- 작성자 : sw.tutor (tutor@swkr.com) 
- 작성일 : 2020.08.05 
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.util.*, java.text.*" %>
 

<%
    Date  nowDate = new Date();
    SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
    String strdate = simpleDate.format(nowDate);
    
%>


<html>
<head>
	<meta charset="utf-8">
	<title>Today-JSP</title> 
</head>
<body>
	<h2>오늘 날자 안내 - JSP</h2>
	<hr>
	오늘은 <b><%=strdate%></b>일 입니다.
</body>
</html>
