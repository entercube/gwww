# ebaeum readme 

1. 개발환경 
- OpenJDK 1.8.0 + Tomcat8.5 
- git 주소 : https://gitlab.com/entercube/gwww 

# 주요환경  
- java connection pooling
- log4j  

# 기능체크 1 : 세션 
: 로컬 
http://localhost:8080/gwww/check.jsp

: 서버  ( war : ROOT.war로 배포 ) 
http://localhost:8080/gwww/check.jsp

# 기능체크 2 : DB기능연동 
: 로컬 
http://localhost:8080/gwww/view/sampleList.jsp
http://localhost:8080/gwww/view/sampleList.jsp?sung=김
http://localhost:8080/gwww/view/sampleList.jsp?sung=박

: 서버 
https://svweb.nuriblock.com/view/sampleList.jsp
https://svweb.nuriblock.com/view/sampleList.jsp?sung=박
https://svweb.nuriblock.com/view/sampleList.jsp?sung=최

: 성명을 누르면 다음링크로 넘어가서 랜덤인명 상세정보 보임 
https://svweb.nuriblock.com/view/sampleView.jsp?rcd=R7046

# 소스배포 
tomcat 서버  : 
cd /svdata/tomcat 
