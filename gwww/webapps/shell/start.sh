#!/bin/bash
# 소스배포 
cd /svdata/tomcat/webapps
sudo /etc/init.d/tomcat8 stop 
sudo rm -rf  ROOT 
sudo wget https://s3.ap-northeast-2.amazonaws.com/cloud.joeunname.co.kr/deploy/ROOT.war
sudo /etc/init.d/tomcat8  start 
sudo chmod 755 ROOT 
echo " === FINISH DEPLOYMENT === "



# 정상 배포 ( for www )
sudo /etc/init.d/tomcat8 stop 
sudo rm -rf  ROOT 
sudo wget https://s3.ap-northeast-2.amazonaws.com/cloud.joeunname.co.kr/deploy/www_ROOT.war
sudo mv www_ROOT.war ROOT.war 
sudo /etc/init.d/tomcat8 start 
sudo rm -rf ROOT.war 
sudo chmod 755 ROOT 
echo " === FINISH DEPLOYMENT === "  