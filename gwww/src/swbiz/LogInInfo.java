package swbiz;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import swedu.util.DBConnection;

public class LogInInfo {
	
	 
    private Connection          conn 		= null;						// 데이터베이스 접속 Connection 객체
    private PreparedStatement   pstmt 		= null;						// 데이터베이스 접속 PreparedStatement  객체
    private ResultSet 			rs 			= null;						// 데이터베이스 접속 Recordset 객체 
    

    /**
     * 기본생성자 
     */
    public LogInInfo() {
    	
    }
    
   	/**
   	 * DB접속설정 
   	 * @param conmode
   	 */
   	protected void setDBConn(int conmode) throws Exception {
   		
        // DB 접속호출 
		if (conmode == DBConnection.POOL_CONNECT) 		conn = DBConnection.getDBContainer();
		else if (conmode == DBConnection.LOCAL_CONNECT) conn = DBConnection.getLocalConnection();
		else if (conmode == DBConnection.TEST_CONNECT)  conn = DBConnection.getTestConnection();
		else conn = DBConnection.getTestConnection();
		
   	}
    
	
    /**
     * 로그인 계정 가져오기
     * email과 pwd를 인자로 받으면 사용자 정보 가져옴
     */
    public LogInInfoDAO getLogInInfo(String Email, String PWD ) throws Exception  {

        String 				    sql 			= null;
        LogInInfoDAO			LTD				= null;
        int						pint			= 1; 

		try {

			// DB기본접속 
			setDBConn(5);
	 
	        // SQL 생성 
			  
	        sql = " SELECT name, mail, pwd, compid" + 
	        	  " FROM s_sample  WHERE mail = ? and pwd = ?";
	   
	        // System.out.println( this.getClass().getName() +  "  RcdVal=" + RcdVal + "/ SQL=" + sql); 
			 
	        pstmt = conn.prepareStatement(sql);
	        
	        // 파라미터 설정 
	        pstmt.setString(pint++, Email);
	        pstmt.setString(pint++, PWD);
	        
	        rs = pstmt.executeQuery();

	        if ( rs.next()) {
	            LTD = new LogInInfoDAO();
	            this.setResultDAO(rs, LTD ); // 설정 
	        }
	        
	        // 접속종료 
	        rs.close();
	        pstmt.close();
	        conn.close();
    
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		
		
        return LTD;

    }
    // EOF getRecord
    
    /**
     * 이메일 정보 가져오기 기본적으로 gmail.com 검색
     */
    public Vector<LogInInfoDAO> getEmailMemberInfo(String email) throws Exception  {

        Vector<LogInInfoDAO>		LSV				= null;
        String 					sql 			= null;
        LogInInfoDAO				LTD				= null;
        int						pint			= 1; 
         
		try {
 
			// DB기본접속 
			setDBConn(5);
	  
			sql = " SELECT name, mail, pwd, compid" + 
		        	  " FROM s_sample WHERE mail like ? ";
		        
	        // P2. 명령어 생성 
	        pstmt = conn.prepareStatement(sql);
	        
	        // P3. 파라미터 설정 
	        pstmt.setString(pint++, email + "%");
 
	        // P4. 레코드셑 호출 
	        rs = pstmt.executeQuery();
	   
	        rs.last();

	        LSV = new  Vector<LogInInfoDAO>(rs.getRow()); // 레코드 수만큼 벡터메모리 할당

	        rs.beforeFirst();

	        while ( rs.next()) {
	        	
	            LTD = new LogInInfoDAO();
	            // DAO 설정 
	            this.setResultDAO(rs, LTD ); 
	            
	            // 벡터콜렉션에 데이터셑 추가 
	            LSV.addElement(LTD);
	        }

	        // 레코드셑, 명렁 및 접속닫기 
	        rs.close();
	        pstmt.close();
	        conn.close();
		 
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		 
        return LSV;

    }
    

	/**
     * 결과셑설정 
     * @param rs
     * @param LTD
     * @throws Exception
     */
    private void setResultDAO(ResultSet rs, LogInInfoDAO LTD ) throws Exception  {

        LTD.name    	= rs.getString("name");
        LTD.mail   		= rs.getString("mail");
        LTD.pwd    		= rs.getString("pwd");
        LTD.compid    	= rs.getInt("compid");
         
    } 
    // End of setResultDAO 
 
}
// End of Class 
