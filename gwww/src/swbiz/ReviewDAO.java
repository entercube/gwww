package swbiz;

import java.io.Serializable;



/**
 * ID : SWEDU-BIZ-3050-JD
 * 기능 : 가게 리뷰 정보처리 데이터 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class ReviewDAO  implements Serializable {



	private static final long serialVersionUID = 343425613321716L;
 
    public int   review_id       = 0;			// 리뷰번호 

    public int   shopCode        = 0;			// 가게번호, FK shopinfo 테이블 참조

    public String   comment      	= null;			// 리뷰
    
    public int   score      	= 0;			// 1~5점 
    
	
	
	@Override
	public String toString() {
		return "ReviewDAO [review_id=" + review_id + ", shopCode=" + shopCode + ", comment=" + comment + ", score=" + score  +"]";
	}
	
}