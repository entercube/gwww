package swbiz;

import java.io.Serializable;



/**
 * ID : SWEDU-BIZ-3010-JD
 * 기능 : 고객 정보처리 데이터 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class CustomerDAO  implements Serializable {

	private static final long serialVersionUID = 787815613321716L;
 
    public String   rcd        	= null;

    public String   name        = null;

    public String   mail      	= null;
    

	public String   phone      	= null;
    
    public String   pwd      	= null;
 
    @Override
	public String toString() {
		return "CustomerDAO [rcd=" + rcd + ", name=" + name + ", mail=" + mail + ", phone=" + phone + ", pwd=" + pwd
				+ "]";
	}

}
