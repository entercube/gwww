package swbiz;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import swedu.util.DBConnection;




/**
 * ID : SWEDU-BIZ-3010-JB
 * 기능 : 고객 정보처리 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class Product1 {
	
	 
    private Connection          conn 		= null;						// 데이터베이스 접속 Connection 객체
    private PreparedStatement   pstmt 		= null;						// 데이터베이스 접속 PreparedStatement  객체
    private ResultSet 			rs 			= null;						// 데이터베이스 접속 Recordset 객체 
 
    private String newRcd					= null; 					// 신규생성 랜덤번호 
    
    
    /**
     * 기본생성자 
     */
    public Product1() {
    	
    }
    
     
    
   	/**
   	 * DB접속설정 
   	 * @param conmode
   	 */
   	protected void setDBConn(int conmode) throws Exception {
   		
        // DB 접속호출 
		if (conmode == DBConnection.POOL_CONNECT) 		conn = DBConnection.getDBContainer();
		else if (conmode == DBConnection.LOCAL_CONNECT) conn = DBConnection.getLocalConnection();
		else if (conmode == DBConnection.TEST_CONNECT)  conn = DBConnection.getTestConnection();
		else conn = DBConnection.getTestConnection();
		
   	}
    
   	

	/**
	 * 제품목록 등록 
	 * @param SQD
	 * @param conmode
	 * @return
	 * @throws Exception
	 */
	public int addRecord( Product1DAO LTD, int conmode ) throws Exception  {

	
		int rcnt				= 0;				// 등록레코드 수 ( 신규 : 1 )
		int pint				= 1;				// 파라미터 순번
 
		
		return rcnt;

	}
	// End of addRecord
    
	
    /**
     * 단일 데이터 가져오기
     * @param lcustid : 고객ID
     * @param conmode : DB접속자 
     * @return
     * @throws Exception
     */
    public Product1DAO getRecord(String productCode, int conmode) throws Exception  {

        String 				    sql 			= null;
        Product1DAO				LTD				= null;
        int						pint			= 1; 
        

		try {

			// DB기본접속 
			setDBConn(conmode);
	 
	        // SQL 생성 
		
	        sql = " SELECT productCode, productName, productLine  \n" + 
	        	 
	        	  " FROM products  WHERE productCode = ? ";
	   
	        // System.out.println( this.getClass().getName() +  "  RcdVal=" + RcdVal + "/ SQL=" + sql); 
			 
	        pstmt = conn.prepareStatement(sql);
	        
	        // 파라미터 설정 
	        pstmt.setString(pint++, productCode);

	        rs = pstmt.executeQuery();

	        if ( rs.next()) {
	            LTD = new Product1DAO();
	            this.setResultDAO(rs, LTD ); // 설정 
	        }
	        
	        // 접속종료 
	        rs.close();
	        pstmt.close();
	        conn.close();
    
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		
		
        return LTD;

    }
    // EOF getRecord 
    
     

    /**
     * 고객성씨에 해당하는 목록 호출 (접속인자 선택) 
     * @param sungStr
     * @param conmode : DB접속자 
     * @return
     * @throws Exception
     */
    public Vector<Product1DAO> getRecordAll(String productLine, int conmode) throws Exception  {

        Vector<Product1DAO>		LSV				= null;
        String 					sql 			= null;
        Product1DAO				LTD				= null;
        int						pint			= 1; 
         
		try {
 
			// DB기본접속 
			setDBConn(conmode);
	  
	        sql = " SELECT productCode, productName, productLine FROM products WHERE productLine like ?";
	        
	        // P2. 명령어 생성 
	        pstmt = conn.prepareStatement(sql);
	        
	        // P3. 파라미터 설정 
	        pstmt.setString(pint++, productLine + "%");
	 
	        // P4. 레코드셑 호출 
	        rs = pstmt.executeQuery();
	   
	        rs.last();

	        LSV = new  Vector<Product1DAO>(rs.getRow()); // 레코드 수만큼 벡터메모리 할당

	        rs.beforeFirst();

	        while ( rs.next()) {
	        	
	            LTD = new Product1DAO();
	            // DAO 설정 
	            this.setResultDAO(rs, LTD ); 
	            
	            // 벡터콜렉션에 데이터셑 추가 
	            LSV.addElement(LTD);
	        }

	        // 레코드셑, 명렁 및 접속닫기 
	        rs.close();
	        pstmt.close();
	        conn.close();
		 
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		 
        return LSV;

    }
    // EOF getRecordAll 
    

    /**
     * 신규생성  랜덤코드 
     * @param conmode
     * @return
     * @throws Exception
     */
    public String getRcd() throws Exception  {
		return this.newRcd; 
	}
 
	/**
     * 결과셑설정 
     * @param rs
     * @param LTD
     * @throws Exception
     */
    private void setResultDAO(ResultSet rs, Product1DAO LTD ) throws Exception  {

        LTD.productCode    		= rs.getString("productCode");
        LTD.productName    	    = rs.getString("productName");
        LTD.productLine   		= rs.getString("productLine");
     
    } 
    // End of setResultDAO 
 
}
// End of Class 
