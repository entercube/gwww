package swbiz;

import java.io.Serializable;



/**
 * ID : SWEDU-BIZ-3050-JD
 * 기능 : 샘플 정보처리 데이터 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class SampleDAO  implements Serializable {



	private static final long serialVersionUID = 4433225613321716L;
 
    public String   rcd        	= null;			// 랜덤코드 

    public String   name        = null;			// 성명 

    public String   mail      	= null;			// 이메일 
    
    public String   pwd      	= null;			// 비밀번호 
    
    public int   	compid      = 0;			// 사원번호 

	public String   phone      	= null;			// 전화번호 
    
	public String   sex      	= null;			// 성별 
	
	public String   remark      = null;			// 주석 

	@Override
	public String toString() {
		return "SampleDAO [rcd=" + rcd + ", name=" + name + ", mail=" + mail + ", pwd=" + pwd + ", compid=" + compid
				+ ", phone=" + phone + ", sex=" + sex + ", remark=" + remark + "]";
	}
	
 
	
}
