package swbiz;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import swedu.util.DBConnection;

public class Shop {
	    private Connection          conn 		= null;						// 데이터베이스 접속 Connection 객체
	    private PreparedStatement   pstmt 		= null;						// 데이터베이스 접속 PreparedStatement  객체
	    private ResultSet 			rs 			= null;						// 데이터베이스 접속 Recordset 객체 
	 
	    private String newRcd					= null; 					// 신규생성 랜덤번호 
	    
	    
	    /**
	     * 기본생성자 
	     */
	    public Shop() {
	    	
	    }
	    
	   	/**
	   	 * DB접속설정 
	   	 * @param conmode
	   	 */
	   	protected void setDBConn(int conmode) throws Exception {
	   		
	        // DB 접속호출 
			if (conmode == DBConnection.POOL_CONNECT) 		conn = DBConnection.getDBContainer();
			else if (conmode == DBConnection.LOCAL_CONNECT) conn = DBConnection.getLocalConnection();
			else if (conmode == DBConnection.TEST_CONNECT)  conn = DBConnection.getTestConnection();
			else conn = DBConnection.getTestConnection();
			
	   	}
	    
	   	
		
	    /**
	     * 단일 데이터 가져오기
	     * @param lcustid : 고객ID
	     * @param conmode : DB접속자 
	     * @return
	     * @throws Exception
	     */
	    public ShopDAO getRecord(String nameVal, int conmode) throws Exception  {

	        String 				    sql 			= null;
	        ShopDAO				LTD				= null;
	        int						pint			= 1; 
	        

			try {

				// DB기본접속 
				setDBConn(conmode);
		 
		        // SQL 생성 	  
		        sql = " SELECT shopCode, shopName, shopCategory, shoplocation, shopPhone\n" + 
			        	  " FROM shopinfo WHERE shopName = ? ";
		   
		    
				 
		        pstmt = conn.prepareStatement(sql);
		        
		        // 파라미터 설정 
		        pstmt.setString(pint++, nameVal);

		        rs = pstmt.executeQuery();

		        if ( rs.next()) {
		            LTD = new ShopDAO();
		            this.setResultDAO(rs, LTD ); // 설정 
		        }
		        
		        // 접속종료 
		        rs.close();
		        pstmt.close();
		        conn.close();
	    
			} 
			catch (Exception e) {
				// 접속객체 오류시 삭제 
				if ( conn != null ) conn.close();
				e.printStackTrace();
			} 
			
			
	        return LTD;

	    }
	    // EOF getRecord 
	    
	     

	    /**
	     * 카테고리에 해당하는 목록 호출 (음식 카테고리 선택) 
	     * @param varStr
	     * @param conmode : DB접속자 
	     * @return
	     * @throws Exception
	     */
	    public Vector<ShopDAO> getRecordAll(String varStr, int conmode) throws Exception  {

	        Vector<ShopDAO>		LSV				= null;
	        String 					sql 			= null;
	        ShopDAO				LTD				= null;
	        int						pint			= 1; 
	         
			try {
	 
				// DB기본접속 
				setDBConn(conmode);
		  
			     sql = " SELECT shopCode, shopName, shopCategory, shoplocation, shopPhone\n" + 
			        	  " FROM shopinfo WHERE shopCategory like ? ";
		   		        
		        // P2. 명령어 생성 
		        pstmt = conn.prepareStatement(sql);
		        
		        // P3. 파라미터 설정 
		        pstmt.setString(pint++, varStr + "%");
		 
		        // P4. 레코드셑 호출 
		        rs = pstmt.executeQuery();
		   
		        rs.last();

		        LSV = new  Vector<ShopDAO>(rs.getRow()); // 레코드 수만큼 벡터메모리 할당

		        rs.beforeFirst();

		        while ( rs.next()) {
		        	
		            LTD = new ShopDAO();
		            // DAO 설정 
		            this.setResultDAO(rs, LTD ); 
		            
		            // 벡터콜렉션에 데이터셑 추가 
		            LSV.addElement(LTD);
		        }

		        // 레코드셑, 명렁 및 접속닫기 
		        rs.close();
		        pstmt.close();
		        conn.close();
			 
			} 
			catch (Exception e) {
				// 접속객체 오류시 삭제 
				if ( conn != null ) conn.close();
				e.printStackTrace();
			} 
			 
	        return LSV;

	    }
	    // EOF getRecordAll 
	    
	    


		/**
	     * 결과셑설정 
	     * @param rs
	     * @param LTD
	     * @throws Exception
	     */
	    private void setResultDAO(ResultSet rs, ShopDAO LTD ) throws Exception  {

	        LTD.shopCode        = rs.getInt("shopCode");
	        LTD.shopName    	= rs.getString("shopName");
	        LTD.shopCategory    = rs.getString("shopCategory");
	        LTD.shoplocation    = rs.getString("shoplocation");
	        LTD.shopPhone    	= rs.getString("shopPhone");
 
	    } 
	    // End of setResultDAO 
	 
	}
	// End of Class 

