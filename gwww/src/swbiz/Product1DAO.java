package swbiz;

import java.io.Serializable;



/**
 * ID : SWEDU-BIZ-3050-JD
 * 기능 : 샘플 정보처리 데이터 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class Product1DAO  implements Serializable {



	private static final long serialVersionUID = 5533225613321716L;
 
    public String   productCode        	= null;			// 랜덤코드 

    public String   productName        = null;			// 성명 

    public String   productLine      	= null;			// 이메일 

	@Override
	public String toString() {
		return "Product1DAO [productCode=" + productCode + ", productName=" + productName + ", productLine="
				+ productLine + "]";
	}
    
  
	

	
}
