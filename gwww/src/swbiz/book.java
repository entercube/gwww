package swbiz;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import swedu.util.DBConnection;

/**
 * ID : book_SELECT
 * 기능 : 도서 조회
 * @version : 1.0  
 * @author : book 
*/ 

public class book {
	 
    private Connection          conn 		= null;						// 데이터베이스 접속 Connection 객체
    private PreparedStatement   pstmt 		= null;						// 데이터베이스 접속 PreparedStatement  객체
    private ResultSet 			rs 			= null;						// 데이터베이스 접속 Recordset 객체 
    private int newRcd						= 0;						// 신규생성 랜덤번호 
    
    /**
     * 기본생성자 
     */
    public book() {
    	
    }
    
   	/**
   	 * DB접속설정 
   	 * @param conmode
   	 */
   	protected void setDBConn(int conmode) throws Exception {
        // DB 접속호출 
		if (conmode == DBConnection.POOL_CONNECT) 		conn = DBConnection.getDBContainer();
		else if (conmode == DBConnection.LOCAL_CONNECT) conn = DBConnection.getLocalConnection();
		else if (conmode == DBConnection.TEST_CONNECT)  conn = DBConnection.getTestConnection();
		else conn = DBConnection.getTestConnection();
   	}
    
	/**
	 * 입력
	 * @param SQD
	 * @param conmode
	 * @return
	 * @throws Exception
	 */
	public int addRecord( bookDAO LTD, int conmode ) throws Exception  {

		int rcnt				= 0;				// 등록레코드 수 ( 신규 : 1 )
		int pint				= 1;				// 파라미터 순번
 
		String sql 				= null;
		String sqlSeq			= null;
  
		try {
 
			// DB기본접속 
			setDBConn(conmode);
			
			sqlSeq = " SELECT max(bookseq) from team3_bookmaster ";
			
	        pstmt = conn.prepareStatement(sqlSeq);
	        rs = pstmt.executeQuery();

	        if ( rs.next()) {
	            LTD = new bookDAO();
	            this.setSeqDAO(rs, LTD ); // 설정 
	        }
	        
	        pstmt = null;
	        rs = null;
	        
			sql = " INSERT INTO team3_bookmaster \n" +
					"( bookseq, bookname, publisher, writer, description, new_flag, best_flag ) \n" +
					" VALUES  \n" + 
					"( ?, ?, ?, ?, ?, ?, ? )";
			
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt	(pint++,	LTD.bookseq);		// 도서순번 
			pstmt.setString (pint++,	LTD.bookname);		// 도서명 
			pstmt.setString (pint++,	LTD.publisher);		// 저자
			pstmt.setString (pint++,	LTD.writer);		// 저자
			pstmt.setString	(pint++,	LTD.description);	// 내용 
			pstmt.setString (pint++,	LTD.new_flag);		// 신간여부
			pstmt.setString (pint++,	LTD.best_flag);		// 베스트셀러여부
			
			rcnt = pstmt.executeUpdate();

			this.newRcd	= LTD.bookseq;
	 
			System.out.println( this.getClass().getName() + " ====================== addRecord================= " ); 
		 
			pstmt.close();
			conn.close();
    
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		 
		return rcnt;

	}
	// End of addRecord
    
	
    /**
     * 단일 데이터 가져오기
     * @param lcustid : 고객ID
     * @param conmode : DB접속자 
     * @return
     * @throws Exception
     */
    public bookDAO getRecord(int bookseq, int conmode) throws Exception  {

        String 				    sql 			= null;
        bookDAO				LTD				= null;
        int						pint			= 1; 
        

		try {

			// DB기본접속 
			setDBConn(conmode);
	 
	        // SQL 생성 
			  
	        sql = " SELECT bookseq, bookname, publisher, writer, description, \n" + 
	        	  " new_flag, best_flag \n" + 
	        	  " FROM team3_bookmaster  WHERE bookseq = ? ";
	   
	        // System.out.println( this.getClass().getName() +  "  RcdVal=" + RcdVal + "/ SQL=" + sql); 
			 
	        pstmt = conn.prepareStatement(sql);
	        
	        // 파라미터 설정 
	        pstmt.setInt(pint++, bookseq);

	        rs = pstmt.executeQuery();

	        if ( rs.next()) {
	            LTD = new bookDAO();
	            this.setResultDAO(rs, LTD ); // 설정 
	        }
	        
	        // 접속종료 
	        rs.close();
	        pstmt.close();
	        conn.close();
    
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		
		
        return LTD;

    }
    // EOF getRecord 
    
    
    /**
     * 순번 가져오기
     * @param lcustid : 고객ID
     * @param conmode : DB접속자 
     * @return
     * @throws Exception
     */
    public bookDAO getBookSeq(int conmode) throws Exception  {

        String 				    sql 			= null;
        bookDAO				LTD				= null;
        int						pint			= 1; 
        

		try {

			// DB기본접속 
			setDBConn(conmode);
	 
	        // SQL 생성 
			  
	        sql = " SELECT bookseq, bookname, publisher, writer, description, \n" + 
		        	  " new_flag, best_flag \n" + 
		        	  " FROM team3_bookmaster  WHERE bookseq = (select max(bookseq) from team3_bookmaster) ";
	   
	        // System.out.println( this.getClass().getName() +  "  RcdVal=" + RcdVal + "/ SQL=" + sql); 
			 
	        pstmt = conn.prepareStatement(sql);
	        rs = pstmt.executeQuery();

	        if ( rs.next()) {
	            LTD = new bookDAO();
	            this.setResultDAO(rs, LTD ); // 설정 
	        }
	        
	        // 접속종료 
	        rs.close();
	        pstmt.close();
	        conn.close();
    
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		
        return LTD;

    }
     

    /**
     * 도서 전체 조회 
     * @param sungStr
     * @param conmode : DB접속자 
     * @return
     * @throws Exception
     */
    public Vector<bookDAO> getRecordAll(String sungStr, int conmode) throws Exception  {

        Vector<bookDAO>		LSV				= null;
        String 					sql 			= null;
        bookDAO				LTD				= null;
        int						pint			= 1; 
         
		try {
 
			// DB기본접속 
			setDBConn(conmode);
	  
	        sql = " SELECT bookseq, bookname, publisher, writer, description, \n" + 
	        	  " new_flag, best_flag \n" + 
	        	  " FROM team3_bookmaster WHERE bookname like ? ";
	        
	        // P2. 명령어 생성 
	        pstmt = conn.prepareStatement(sql);
	        
	        // P3. 파라미터 설정 
	        pstmt.setString(pint++, sungStr + "%");
	 
	        // P4. 레코드셑 호출 
	        rs = pstmt.executeQuery();
	   
	        rs.last();

	        LSV = new  Vector<bookDAO>(rs.getRow()); // 레코드 수만큼 벡터메모리 할당

	        rs.beforeFirst();

	        while ( rs.next()) {
	        	
	            LTD = new bookDAO();
	            // DAO 설정 
	            this.setResultDAO(rs, LTD ); 
	            
	            // 벡터콜렉션에 데이터셑 추가 
	            LSV.addElement(LTD);
	        }

	        // 레코드셑, 명렁 및 접속닫기 
	        rs.close();
	        pstmt.close();
	        conn.close();
		 
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		 
        return LSV;

    }
    // EOF getRecordAll 
    
    /**
     * 도서 전체 조회 (신간)
     * @param 
     * @param conmode : DB접속자 
     * @return
     * @throws Exception
     */
    public Vector<bookDAO> getRecordAllNew(int conmode) throws Exception  {

        Vector<bookDAO>			LSV				= null;
        String 					sql 			= null;
        bookDAO					LTD				= null;
        int						pint			= 1; 
         
		try {
 
			// DB기본접속 
			setDBConn(conmode);
	  
	        sql = " SELECT bookseq, bookname, publisher, writer, description, \n" + 
	        	  " new_flag, best_flag \n" + 
	        	  " FROM team3_bookmaster WHERE new_flag = 'Y' ";
	        
	        // P2. 명령어 생성 
	        pstmt = conn.prepareStatement(sql);
	        
	        // P4. 레코드셑 호출 
	        rs = pstmt.executeQuery();
	   
	        rs.last();

	        LSV = new  Vector<bookDAO>(rs.getRow()); // 레코드 수만큼 벡터메모리 할당

	        rs.beforeFirst();

	        while ( rs.next()) {
	        	
	            LTD = new bookDAO();
	            // DAO 설정 
	            this.setResultDAO(rs, LTD ); 
	            
	            // 벡터콜렉션에 데이터셑 추가 
	            LSV.addElement(LTD);
	        }

	        // 레코드셑, 명렁 및 접속닫기 
	        rs.close();
	        pstmt.close();
	        conn.close();
		 
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		 
        return LSV;

    }
    // EOF getRecordAllBest
    
    /**
     * 도서 전체 조회 (베스트셀러)
     * @param 
     * @param conmode : DB접속자 
     * @return
     * @throws Exception
     */
    public Vector<bookDAO> getRecordAllBest(int conmode) throws Exception  {

        Vector<bookDAO>		LSV				= null;
        String 					sql 			= null;
        bookDAO				LTD				= null;
        int						pint			= 1; 
         
		try {
 
			// DB기본접속 
			setDBConn(conmode);
	  
	        sql = " SELECT bookseq, bookname, publisher, writer, description, \n" + 
	        	  " new_flag, best_flag \n" + 
	        	  " FROM team3_bookmaster WHERE best_flag = 'Y' ";
	        
	        // P2. 명령어 생성 
	        pstmt = conn.prepareStatement(sql);
	        
	        // P4. 레코드셑 호출 
	        rs = pstmt.executeQuery();
	   
	        rs.last();

	        LSV = new  Vector<bookDAO>(rs.getRow()); // 레코드 수만큼 벡터메모리 할당

	        rs.beforeFirst();

	        while ( rs.next()) {
	        	
	            LTD = new bookDAO();
	            // DAO 설정 
	            this.setResultDAO(rs, LTD ); 
	            
	            // 벡터콜렉션에 데이터셑 추가 
	            LSV.addElement(LTD);
	        }

	        // 레코드셑, 명렁 및 접속닫기 
	        rs.close();
	        pstmt.close();
	        conn.close();
		 
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		 
        return LSV;

    }
    // EOF getRecordAllBest
    

    /**
     * 신규생성  랜덤코드 
     * @param conmode
     * @return
     * @throws Exception
     */
    public int getRcd() throws Exception  {
		return this.newRcd; 
	}
 
	/**
     * 결과셑설정 
     * @param rs
     * @param LTD
     * @throws Exception
     */
    private void setResultDAO(ResultSet rs, bookDAO LTD ) throws Exception  {
    	
        LTD.bookseq		= rs.getInt("bookseq");
        LTD.bookname	= rs.getString("bookname");
        LTD.publisher	= rs.getString("publisher");
        LTD.writer		= rs.getString("writer");
        LTD.description	= rs.getString("description");
        LTD.new_flag	= rs.getString("new_flag");
        LTD.best_flag	= rs.getString("best_flag");
        
    }
    // End of setResultDAO 
 
	/**
     * 결과셑설정 
     * @param rs
     * @param LTD
     * @throws Exception
     */
    private void setSeqDAO(ResultSet rs, bookDAO LTD ) throws Exception  {
        LTD.bookseq		= rs.getInt("bookseq");
    }
    // End of setResultDAO 
}
// End of Class 
