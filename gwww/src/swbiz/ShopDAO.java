package swbiz;

import java.io.Serializable;



/**
 * ID : SWEDU-BIZ-3050-JD
 * 기능 : 가게 정보처리 데이터 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class ShopDAO  implements Serializable {



	private static final long serialVersionUID = 40442213321716L;
 
    public int   shopCode       = 0;			// 랜덤코드 

    public String   shopName        = null;			// 가게명

    public String   shopCategory      	= null;			// 가게 종류 
    
    public String   shoplocation      	= null;			// 가게 위치
    
    public String   shopPhone      	= null;			// 가게 연락처
    
  
	
	
	@Override
	public String toString() {
		return "ShopDAO [shopCode=" + shopCode + ", shopName=" + shopName + ", shopCategory=" + shopCategory + ", shoplocation=" + shoplocation 
				+ ", shopPhone=" + shopPhone +"]";
	}
	
}