package swbiz;

import java.io.Serializable;



/**
 * ID : SWEDU-BIZ-3020-JD
 * 기능 : 도움요청 데이터 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class helpdeskDAO  implements Serializable {

	private static final long serialVersionUID = 9089545888999L;
	
    public int      helpid          = 1000;

    public String   helpcode        = null;

    public String   hcatmain        = null;

    public String   hcatdetail      = null;

    public String   helptitle       = null;

    public String   helpbody        = null;

    public String   hname           = null;

    public String   hemail          = null;

    public String   hphone          = null;

    public String   hpdate          = null;

    public String   hptime          = null;

    public String   replyname       = null;

    public String   replytitle      = null;

    public String   replybody       = null;

    public String   rpdate          = null;

    public String   rptime          = null;

    public String   remark          = null;

    // 2016.11.22 신규 추가
    public String   codecat         = null;

    public String   ptcode          = null;

    // 2016.11.24 신규 추가

    public String   phelpid         = null;
    public int	    rcnt        	= 0;		// 조회수 
    public int      helpflag        = 0;
    
    // 파일 플래그, 명칭 및 타입 
    public String 	cfiflag			= null;		// 요청파일플래그 ( Y : 첨부있음, N : 첨부없음 )    
    public String 	cfiname			= null;		// 요청파일명칭   
    public String 	cfitype			= null;  	// 요청파일타입 
    
    public String 	rfiflag			= null; 	// 답변파일플래그 ( Y : 첨부있음, N : 첨부없음 )     
    public String 	rfiname			= null;  	// 답변파일명칭   
    public String 	rfitype			= null; 	// 답변파일타입  
    
    public String 	menutitle		= null; 	// 기능제목 ( 예:지원요청, 사용문의, 제휴문의, 문제신고.. 등 )
    
    public String   userip        	= null;		// 등록자 ip 
    
    public String   useripView  	= null;		 // 도움 등록 IP보이기 ( ex : 2**.1**
    
    // 2018. 1. 2 : 커뮤니케이션 준비 
    public String   hpasswd  		= null;		 // 도움요청 등록암호
    
    public String   openupd  		= "Y";		 // 공개여부 ( Y:공개, N:비공개) 
    
    public String   menucode		= null;		 // 
    
 //  qa : 질문답변  / help : 지원요청 / speak : 공지사항 / use : 사용후기 / free : 자유게시판  
 	
    
    public int      depth       	= 0;		// 0 : 기본 / 1 : 1차 댓글, 2 : 2차 댓글 
    
    // mbid, mcd, acd, ccd, cqcd, tid, qno 
    
    // 참조키값 목록 
    public int     	mbid		= 0;		 	// 회원id',
    public String   mcd			= null;		 	// 출제자코드';
    public String   acd			= null;		 	// 응답코드 CCD-CQCD-TID-QTYPE-QNO';
    public String   ccd			= null;		 	// 과정코드';
    public int      cqcd       	= 0;			// 과정문제셑코드';
    public int      tid       	= 0;			// 답변자아이디';
    public int     	qno       	= 0;			// 문항번호';
   
    // 조회일자 시각 
    public String   vdate			= null;		 	// 조회일자
    public String   vtime			= null;		 	// 조회시간 
    
}
