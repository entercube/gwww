package swbiz;

import java.io.Serializable;

public class LogInInfoDAO  implements Serializable {


	private static final long serialVersionUID = 4433123453321716L;
 
	
    public String   name        = null;			// 성명 

    public String   mail      	= null;			// 이메일 
    
    public String   pwd      	= null;			// 비밀번호 
    
    public int   	compid      = 0;			// 사원번호 
	
	
	@Override
	public String toString() {
		return "LogInInfoDAO [name=" + name + ", mail=" + mail + ", pwd=" + pwd + ", compid=" + compid+ "]";
	}
	
}
