package swbiz;

import java.io.Serializable;

/**
 * ID : book_SELECT
 * 기능 : 도서 정보 조회 
 * @version : 1.0
 * @author : book 
*/ 

public class bookDAO implements Serializable {

	private static final long serialVersionUID = 4433225613321716L;
 
    public int		bookseq		= 0;	// 도서순번 
    public String	bookname	= null;	// 도서명
    public String	publisher	= null;	// 출판사 
    public String	writer		= null;	// 저자
    public String	description	= null;	// 내용
	public String	new_flag	= null;	// 신간여부
	public String	best_flag	= null;	// 베스트여부
	
	@Override
	public String toString() {
		return "bookDAO [bookseq=" + bookseq + ", bookname=" + bookname + ", publisher=" + publisher + ", writer=" + writer + ", description=" + description
				+ ", new_flag=" + new_flag + ", best_flag=" + best_flag + "]";
	}
}