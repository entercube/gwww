package swbiz;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import swedu.util.DBConnection;




/**
 * ID : SWEDU-BIZ-3010-JB
 * 기능 : 고객 정보처리 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class Customer {
	
	 
    private Connection          conn 		= null;						// 데이터베이스 접속 Connection 객체
    private PreparedStatement   pstmt 		= null;						// 데이터베이스 접속 PreparedStatement  객체
    private ResultSet 			rs 			= null;						// 데이터베이스 접속 Recordset 객체 
 
    private String newCustId				= null; 					// 신규생성 고객번호 
    
    
    /**
     * 기본생성자 
     */
    public Customer() {
    }
    
    
    /**
     * 헬프데스크 항목 가져오기
     * @param lhelpid 
     * @param conmode
     * @return
     * @throws Exception
     */
    public CustomerDAO getRecord(String custId) throws Exception  {

        String 				    sql 			= null;
        CustomerDAO				LTD				= null;
        int						pint			= 1; 
        

		try {

	        // DB 접속호출 
	        conn = DBConnection.getTestConnection(); 

	        // SQL 생성 
	        sql = "SELECT rcd, name, mail, pwd, ifnull(phone,'') as phone FROM s_random WHERE rcd = ? ";
	  
	        pstmt = conn.prepareStatement(sql);
	        
	        // 파라미터 설정 
	        pstmt.setString(pint++, custId);

	        rs = pstmt.executeQuery();

	        if ( rs.next()) {
	            LTD = new CustomerDAO();
	            this.setResultDAO(rs, LTD ); // 설정 
	        }
	        
	        // 접속종료 
	        rs.close();
	        pstmt.close();
	        conn.close();
    
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			if ( conn != null ) conn.close();
			e.printStackTrace();
		} 
		
		
        return LTD;

    }
    // EOF getRecord 
     
    /**
     * 고객성씨에 해당하는 목록 호출 
     * @param sungStr
     * @return
     * @throws Exception
     */
    public Vector<CustomerDAO> getRecordAll(String sungStr) throws Exception  {

        Vector<CustomerDAO>		LSV				= null;
        String 					sql 			= null;
        CustomerDAO				LTD				= null;
        int						pint			= 1; 
        

		try {
 
	        // DB 접속호출 
	        conn = DBConnection.getTestConnection(); 

	        sql = "SELECT rcd, name, mail, pwd, ifnull(phone,'') as phone FROM s_random WHERE name like ? ";
	        
	        // P2. 명령어 생성 
	        pstmt = conn.prepareStatement(sql);
	        
	        // P3. 파라미터 설정 
	        pstmt.setString(pint++, sungStr + "%");
	 
	        // P4. 레코드셑 호출 
	        rs = pstmt.executeQuery();
	   
	        rs.last();

	        LSV = new  Vector<CustomerDAO>(rs.getRow()); // 레코드 수만큼 벡터메모리 할당

	        rs.beforeFirst();

	        while ( rs.next()) {
	        	
	            LTD = new CustomerDAO();
	            // DAO 설정 
	            this.setResultDAO(rs, LTD ); 
	            
	            // 벡터콜렉션에 데이터셑 추가 
	            LSV.addElement(LTD);
	        }

	        // 레코드셑, 명렁 및 접속닫기 
	        rs.close();
	        pstmt.close();
	 
		} 
		catch (Exception e) {
			// 접속객체 오류시 삭제 
			
			e.printStackTrace();
		} 
		finally {
			if ( conn != null ) conn.close();
		}
		 
        return LSV;

    }
    // EOF getRecordAll 
 
    
 
    

    /**
     * 신규생성  회원번호 id반환  
     * @param conmode
     * @return
     * @throws Exception
     */
    public String getCustId() throws Exception  {
		return this.newCustId; 
	}

 

	/**
     * 결과셑설정 
     * @param rs
     * @param LTD
     * @throws Exception
     */
    private void setResultDAO(ResultSet rs, CustomerDAO LTD ) throws Exception  {

        LTD.rcd    		= rs.getString("rcd");
        LTD.name    	= rs.getString("name");
        LTD.mail   		= rs.getString("mail");
        LTD.pwd    		= rs.getString("pwd");
        LTD.phone    	= rs.getString("phone");
         
    } 
    // End of setResultDAO 
 
}
// End of Class 
