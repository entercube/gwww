package swedu.svlt;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 

/**
 * ID : SWEDU-SVT-3050-JB
 * 기능 : 날자보기 서블릿 예제 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class TestSv2 extends HttpServlet {
	
	private static final long serialVersionUID = 1500L;

    public TestSv2() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	    response.setContentType("text/html;charset=utf-8");
	    PrintWriter out = response.getWriter();

	    out.println("<HTML><HEAD><TITLE>날자보기</TITLE></HEAD></HTML>");
	    out.println("<BODY><H2>View Date using Servlet</H2>");
	    out.println("오늘의 일자 정보   : " + new java.util.Date());
	    out.println("</BODY></HTML>");
 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
