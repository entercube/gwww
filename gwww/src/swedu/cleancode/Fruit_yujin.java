package swedu.cleancode; 
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
    작성자 : 김유진
    날짜 : 2024.04.11
 */




public class Fruit_yujin {

    public static final int MONTH_DAYS = 30;
    public static BufferedReader br;
    public static BufferedWriter bw;
    public static SeasonFruit seasonFruit;

    public static final List<Integer> spring = new ArrayList<>(Arrays.asList(3,4,5));
    public static final List<Integer> summer = new ArrayList<>(Arrays.asList(6,7,8));
    public static final List<Integer> autumn = new ArrayList<>(Arrays.asList(9,10,11));
    public static final List<Integer> winter = new ArrayList<>(Arrays.asList(12,1,2));






    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));
        bw = new BufferedWriter(new OutputStreamWriter(System.out));

        // 입력 (월, 명)
        int month = Integer.parseInt(br.readLine());
        int userCount = Integer.parseInt(br.readLine());

        // 몇 월 입력 -> 제철 과일이 달라짐
        seasonFruit = getSeasonFruit(month); // 시즌 과일
        writeOrderSheet(month,userCount, seasonFruit);

        bw.flush();
    }

 
    public static void writeOrderSheet(int month, int userCount, SeasonFruit seasonFruit ) throws IOException {
		
        String serveFruitName;
		
        int lastDay;
		
        for(int day = 1; day <= MONTH_DAYS; day++){
            lastDay = day % 10;
            bw.write(String.format("%d월 %d일 : ", month, day));
            //1, 5로 끝나는지
            if(lastDay == 1 || lastDay == 5){
                // 딸기 혹은 사과
                serveFruitName = seasonFruit.fruit1;
                bw.write(serveFruitName +" " + userCount +"개\n");
            }
            else if(lastDay == 3 || lastDay == 7){
                serveFruitName = seasonFruit.fruit2;
                bw.write(serveFruitName +" " + userCount +"개\n");
            }
            else{
                bw.write("-\n");
            }
        }
    }

    
    /**
     * 계절과일클래스 
     */
	static class SeasonFruit {
		
	    String fruit1;
	    String fruit2;
	
	    public SeasonFruit(String fruit1, String fruit2) {
	        this.fruit1 = fruit1;
	        this.fruit2 = fruit2;
	    }
	}
	
	
	
    public static SeasonFruit getSeasonFruit(int month){
    	
    	SeasonFruit outFruit = null; 
    	 
        if(spring.contains(month) || summer.contains(month)){ // 봄과 여름이라면
        	outFruit = new SeasonFruit("딸기", "수박");
        } 
        else if(autumn.contains(month) || winter.contains(month)){
        	outFruit = new SeasonFruit("수박", "배");
        }
 
        return outFruit;
    }
 

} 


