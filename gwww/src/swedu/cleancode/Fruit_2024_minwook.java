package swedu.cleancode;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

// 김민욱 2024.04.14 실습 (동작성공) 
public class Fruit_2024_minwook {
    private static int studentCount;
    private static int currentMonth;
    private static final int DAY_PER_MONTH = 30;
    private static final int SPRING_START_MONTH = 3;
    private static final int SPRING_END_MONTH = 5;
    private static final int SUMMER_START_MONTH = 6;
    private static final int SUMMER_END_MONTH = 8;
    private static final int FALL_START_MONTH = 9;
    private static final int FALL_END_MONTH = 11;
    private static final int WINTER_START_MONTH = 12;
    private static final int WINTER_END_MONTH = 2;


    public static void main(String[] args) {
        // 초기 학생 수
        studentCount = 71;
        // 초기 월
        currentMonth = 3;

        // 현재 계절
        String currentSeason = getCurrentSeason(currentMonth);

        // 파일 출력을 위한 Writer 생성
        PrintWriter pw = null;
        try {
            pw = new PrintWriter("out.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        pw.println("3월 디저트 구매 내역서 (총 " + studentCount + "명)");

        for (int day = 1; day <= DAY_PER_MONTH; day++) {
            pw.print("3월 " + day + "일 : ");
            if (isDayEndWith1or5(day)) {
                if (currentSeason.equals("SPRING") || currentSeason.equals("SUMMER")) {
                    pw.println("딸기 " + studentCount * 5 + "개");
                } else {
                    pw.println("사과 " + studentCount + "개");
                }
            } else if (isDayEndWith3or7(day)) {
                if (currentSeason.equals("SPRING") || currentSeason.equals("SUMMER")) {
                    pw.println("수박 " + ((studentCount / 10) + (studentCount % 10 == 0 ? 0 : 1)) + "개");
                } else {
                    pw.println("배 " + ((studentCount / 2) + (studentCount % 2 == 0 ? 0 : 1)) + "개");
                }
            } else {
                pw.println();
            }
        }

        pw.close();
    }

    private static boolean isDayEndWith3or7(int day) {
        return day % 10 == 3 || day % 10 == 7;
    }

    private static boolean isDayEndWith1or5(int day) {
        return day % 10 == 1 || day % 10 == 5;
    }

    // 월을 입력받아 현재 계절을 반환
    private static String getCurrentSeason(int currentMonth) {
        if ((SPRING_START_MONTH <= currentMonth) && (currentMonth <= SPRING_END_MONTH)) {
            return "SPRING";
        } else if ((SUMMER_START_MONTH <= currentMonth) && (currentMonth <= SUMMER_END_MONTH)) {
            return "SUMMER";
        } else if ((FALL_START_MONTH <= currentMonth) && (currentMonth <= FALL_END_MONTH)) {
            return "FALL";
        } else
            return "WINTER";
    }
}
