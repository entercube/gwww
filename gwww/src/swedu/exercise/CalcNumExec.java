package swedu.exercise;
 
 
/**
 * ID : Hong-1001-EXEC
 * 기능 : 다양한 숫자연산 실행
 * @author 홍길동(cwjung123@gmail.com) 
 *
 */
public class CalcNumExec {

	
	public static void main(String[] args) throws Exception  {
		
		// 숫자계산 객체 호출 
		CalcNum CN = new CalcNum(); 
		
		int targetNum = 20000;
		int num = 3;
		int times = 400; 
		
		// 최종결과값 
		int resultOut = 0;
		
		System.out.println( "\n\n 1. ============ 다중덧셈 구현 ==============" );
		
		resultOut = CN.numAdd(targetNum, num, times);
		
		System.out.println( "대상값=" + targetNum + " / 처리값=" + num + " / 횟수=" + times );
		
		System.out.println( "결과값=" + resultOut );
		
		System.out.println( "\n\n 2. ============ 다중뺄셈 구현 ==============" );
		
		resultOut = CN.numMinus(targetNum, num, times);
		
		System.out.println( "대상값=" + targetNum + " / 처리값=" + num + " / 횟수=" + times );
		
		System.out.println( "결과값=" + resultOut );
		
		System.out.println( "\n\n 3. ============ 다중덧셈 구현 생성자 2 ==============" );
		
		CN = new CalcNum(1, targetNum, num, times); 
		resultOut = CN.getOutResultNum(); 
		
		System.out.println( "대상값=" + targetNum + " / 처리값=" + num + " / 횟수=" + times );
		System.out.println( "결과값=" + resultOut );

		System.out.println( "\n\n 4. ============ 다중뺄셈 구현 생성자 2 ==============" );
		
		CalcNum CN3 = new CalcNum(2, targetNum, num, times); 
		resultOut = CN3.getOutResultNum(); 
		
		System.out.println( "대상값=" + targetNum + " / 처리값=" + num + " / 횟수=" + times );
		System.out.println( "결과값=" + resultOut );
		
		
		System.out.println( "\n\n 5. ============ 다중덧셈 이중인자  ==============" );
		
		resultOut = CN.numAdd(targetNum, num );
		 
		System.out.println( "대상값=" + targetNum + " / 처리값=" + num + " / 횟수=" + times );
		System.out.println( "결과값=" + resultOut );
		
		
		System.out.println( "\n\n 6 . ============ 다중곱셉  ==============" );
		
		resultOut = CN.numMultiple(targetNum, num, times);
		 
		System.out.println( "대상값=" + targetNum + " / 처리값=" + num + " / 횟수=" + times );
		System.out.println( "결과값=" + resultOut );
		
		
		System.out.println( "\n\n 7 . ============ 다중나누기  ==============" );
		
		resultOut = CN.numMultiple(20000, 4, 12);
		 
		System.out.println( "대상값=" + targetNum + " / 처리값=" + num + " / 횟수=" + times );
		System.out.println( "결과값=" + resultOut );
		 
	}
	// EOF main 
}
// EOF Class 