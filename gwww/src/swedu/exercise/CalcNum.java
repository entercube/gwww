package swedu.exercise;
 
/**
 * ID : Hong-1000
 * 기능 : 다양한 숫자연산 
 * @author 홍길동(cwjung123@gmail.com) 
 *
 */
public class CalcNum  {

	// 결과값 전달 클래스 변수 
	protected int outResultNum = 0;
 
	/**
	 * 기본생성자 
	 */
    public CalcNum() {
    	
    }
    
	/**
	 * 생성자 오버로딩 
	 * @param calcFlag
	 * @param targetNum
	 * @param inNum
	 * @param times
	 * @throws Exception
	 */
    public CalcNum(int calcFlag, int targetNum, int inNum, int times ) throws Exception  {
    	
    	// 계산요청 플래그에 따라서 연산수행 
    	if ( calcFlag == 1 ) {
    		outResultNum = numAdd( targetNum,  inNum,  times);
    	}
    	else if ( calcFlag == 2 ) {
    		outResultNum = numMinus( targetNum,  inNum,  times);
    	}
    	else {
    		outResultNum = -9999; 
    	}
    	 
    }
    
    
    
    /**
     * 화면보이기   ex) 4 x 3 -----> 12 
     * @param n1 ( ex : n1 ) 
     * @param n2 ( ex : n2 ) 
     * @param delimeter ( 중간구분자 x )
     * @param resultNum ( ex : 12 ) 
     * @return
     * @throws Exception
     */
    protected String viewDualNum(int n1, int n2, String delimeter, String resultNum ) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	String outString = null;  
   
		try {
			outString = n1 + delimeter + n2 + " -----> " + resultNum; 
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값으로 공백문자를 던짐 
			
			ex.printStackTrace();
			
			return ""; 
		} 
		
		// 출력값 전달 
		return outString; 

    }
    // EOF numAdd
     
    
    
    /**
     * 다중 숫자 더하기 
     * @param targetNum  	: 입력숫자 1   ex) 20 
     * @param inNum  		: 입력숫자 2   ex) 3
     * @param times 		: 연산횟수    ex) 4 
     * @return  ex) 20 + (3*4) = 32  
     * @throws Exception
     */
    public int numAdd(int targetNum, int inNum, int times) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	int outNum = 0;  
   
		try {
			outNum = targetNum + (inNum * times);
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값으로 -9999을 던짐 
			outNum = -9999; 
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return outNum; 

    }
    // EOF numAdd
     
    
    /**
     * 단일 숫자 더하기 
     * @param targetNum  	: 입력숫자 1   ex) 20 
     * @param inNum  		: 입력숫자 2   ex) 3 
     * @return  ex) 20 + 3 = 23 
     * @throws Exception
     */
    public int numAdd(int targetNum, int inNum) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	int outNum = 0;  
   
		try {
			
			outNum = targetNum + inNum; 
			
			System.out.println( this.getClass().getName() + " / numAdd overloading result=" + outNum );
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값으로 -9999을 던짐 
			outNum = -9999; 
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return outNum; 

    }
    // EOF numAdd
    

    /**
     * 숫자 다중 빼기  
     * @param targetNum  : 입력숫자 1   ex) 20 
     * @param inNum  : 입력숫자 2   ex) 3
     * @param times : 연산횟수    ex) 4 
     * @return  ex) 20 - ( 3 * 4 ) = 8 
     * @throws Exception
     */
    public int numMinus(int targetNum, int inNum, int times) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	int outNum = 0;  
   
		try {
			outNum = targetNum - (inNum * times);
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값으로 -9999을 던짐 
			outNum = -9999; 
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return outNum; 

    }
    // EOF numMinus 
    
    
    /**
     * 숫자를 다중으로 곱하기 함 
     * @param targetNum
     * @param inNum
     * @param times
     * @return
     * @throws Exception
     */
    public int numMultiple(int targetNum, int inNum, int times) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	int outNum = 0;  
   
		try {
			 
			
			
			
			
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값으로 -9999을 던짐 
			outNum = -9999; 
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return outNum; 

    }
    // EOF numMultiple 
    
     
    /**
     * 숫자를 다중으로 나누기 함 
     * @param targetNum
     * @param inNum
     * @param times
     * @return
     * @throws Exception
     */
    public int numDivide(int targetNum, int inNum, int times) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	int outNum = 0;  
   
		try {
			 
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값으로 -9999을 던짐 
			outNum = -9999; 
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return outNum; 

    }
    // EOF numDivide 
    
    
	/**
	 * 생성자 기본 결과값 전달 
	 * @return
	 */
	public int getOutResultNum() {
		return outResultNum;
	}

}
// EOF Class 