package swedu.exercise;
 
/**
 * ID : doori-1002-EXEC
 * 기능 : 구구단 실행 
 * @author 김두리 
 *
 */
public class CalcNum_3doori extends CalcNumNew {
 
	/**
	 * 기본생성자 
	 */
    public CalcNum_3doori() {
    	
    }
     
    /**
     * 구구단 반환 
     * @param lvl : 구구단 레벨 ( 9 or 19 ) 
     * @return
     * @throws Exception
     */
    public String doGuguDan(int lvl) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	String finalResult = "";
    	String oneResult   = ""; 
   
		try {
			// ========= 구구단 로직 시작 ===============
			int x   = 0;   // 구구단 입력 왼편 ex) 3    
	        int y   = 0;    // 구구단 입력 오른편 변수 ex) 7 
	        int z   = 0;    // 결과값 ex) 21 
	        
	        // let lvl = 9;    // 구구단 레벨 ( 9 or 19 ) 
	          
	        for(x=2;x<=lvl;x++) {
	        	 
	           for(y=1;y<=lvl;y++) {
	            	
	               z = x*y;
	               
	               // oneResult = x + " X " + y + " ====> " + z; 
	               
	               oneResult = super.viewDualNum(x, y, " X ", z + "");
	                
	               finalResult += oneResult + "\n";  
	           }
	        }  
	              
			
			// =========== 구구단 로직 종료 ===============
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값은 공백 
			finalResult = "";  
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return finalResult; 

    }
    // EOF doGuguDan
     
     /*
      *   2 X 2 ------> 4 
      *   2 X 3 ------> 6 
      */
}
// EOF Class 