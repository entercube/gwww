package swedu.exercise;

/*
 * @author KLID 실습 테스트 
 * 
 * */

public class CalcNum2 {
	
	//결과값 전달 클레스 변수
	private int outResultNum = 0;
	
	/* 기본 생성자  */
	public CalcNum2() 
	{
    
	}
	
	/* 기본 생성자 overloading */
	public CalcNum2(int calflag, int targetNum, int inputNum, int times) throws Exception
	{
		if(calflag == 1)
		{
			outResultNum = numAdd(targetNum, inputNum, times);
		}
		else if(calflag == 2)
		{
			outResultNum = numMinus(targetNum, inputNum, times);
		}
		else
		{
			outResultNum = numMultiple(targetNum, inputNum, times);
		}
	}
	
	/**
	 * 더하기 
	 * @param targetNum : 입력 숫자 1 ex) 5
	 * @param inputNum : 입력 숫자 2 ex) 4
	 * @param times : 연산 횟수 ex) 3
	 * @return 5 + (4 * 3) = 17
	 * @throws Exception
	 */
	public int numAdd(int targetNum, int inputNum, int times) throws Exception
	{
		//계산된 결과를 반환하는 변수
		int outNum = 0;
		
		try
		{
			// 계산
			outNum = targetNum + ( inputNum  * times);

		}
		catch(Exception e)
		{
			// 오류 발생시에는 오류로그를 출력하고 결과값으로 -1을 전달
			outNum = -999;
			e.printStackTrace();
		
		}
		
		//계산된 결과를 전달
		return outNum;
	}
	
	/**
	 * 더하기 메서드 오버로드
	 * @param targetNum : 입력 숫자 1 ex) 5
	 * @param inputNum : 입력 숫자 2 ex) 4
	 * @return 5 + 4 = 9
	 * @throws Exception
	 */
	public int numAdd(int targetNum, int inputNum) throws Exception
	{
		//계산된 결과를 반환하는 변수
		int outNum = 0;
		
		try
		{
			// 계산
			outNum = targetNum +  inputNum;
			
			System.out.println(this.getClass() + " / numAdd의 결과 값은 ?" + outNum);

		}
		catch(Exception e)
		{
			// 오류 발생시에는 오류로그를 출력하고 결과값으로 -1을 전달
			outNum = -999;
			e.printStackTrace();
		
		}
		
		//계산된 결과를 전달
		return outNum;
	}
	
	
	/**
	 * 뺄셈
	 * @param targetNum : 입력 숫자 1 ex) 20
	 * @param inputNum : 입력 숫자 2 ex) 4
	 * @param times : 연산 횟수 ex) 3
	 * @return 20 - (4 * 3) = 8
	 * @throws Exception
	 */
	public int numMinus(int targetNum, int inputNum, int times) throws Exception
	{
		//계산된 결과를 반환하는 변수
		int outNum = 0;
		
		try
		{
			// 계산
			outNum = targetNum - (inputNum  * times);

		}
		catch(Exception e)
		{
			// 오류 발생시에는 오류로그를 출력하고 결과값으로 -1을 전달
			outNum = -999;
			e.printStackTrace();
		
		}
		
		//계산된 결과를 전달
		return outNum;
	}
	
	/**
	 * 곱하기
	 * @param targetNum : 입력 숫자 1 ex) 20
	 * @param inputNum : 입력 숫자 2 ex) 4
	 * @param times : 연산 횟수 ex) 3
	 * @return 20 - (4 * 3) = 8
	 * @throws Exception
	 */
	public int numMultiple(int targetNum, int inputNum, int times) throws Exception
	{
		//계산된 결과를 반환하는 변수
		int outNum = 0;
		
		try
		{
			// 계산
			outNum = targetNum - (inputNum * times);

		}
		catch(Exception e)
		{
			// 오류 발생시에는 오류로그를 출력하고 결과값으로 -1을 전달
			outNum = -999;
			e.printStackTrace();
		
		}
		
		//계산된 결과를 전달
		return outNum;
	}
	
	/**
	 * 나누기
	 * @param targetNum : 입력 숫자 1 ex) 20
	 * @param inputNum : 입력 숫자 2 ex) 4
	 * @param times : 연산 횟수 ex) 3
	 * @return 20 - (4 * 3) = 8
	 * @throws Exception
	 */
	public double numDivide(int targetNum, int inputNum, int times) throws Exception
	{
		//계산된 결과를 반환하는 변수
		double outNum = 0;
		
		try
		{
			// 계산
			outNum = targetNum % (inputNum * times);

		}
		catch(Exception e)
		{
			// 오류 발생시에는 오류로그를 출력하고 결과값으로 -1을 전달
			outNum = -999;
			e.printStackTrace();
		
		}
		
		//계산된 결과를 전달
		return outNum;
	}
	
	/**
	 * 생성자 기본 결과값 전달 
	 * @return
	 */
	public int getOutResultNum() {
		return outResultNum;
	}
}