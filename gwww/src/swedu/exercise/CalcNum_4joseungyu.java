package swedu.exercise;
 
/**
 * ID : Hong-1010
 * 기능 : 다양한 숫자연산 자식 
 * @author 조승유(seungyujo@klid.or.kr)
 *
 */
public class CalcNum_4joseungyu extends CalcNumNew {
 
	/**
	 * 기본생성자 
	 */
    public CalcNum_4joseungyu() {
    	
    }
     
    /**
     * 구구단 반환 
     * @param lvl : 구구단 레벨 ( 9 or 19 ) 
     * @return
     * @throws Exception
     */
    public String doGuguDan(int lvl) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	String finalResult = "";//최종결과
    	String oneResult   = ""; 
         
		try {
			// ========= 구구단 로직 시작 ===============
			
			int firstNum	= 0;	// 구구단 입력 왼편 ex) 3 	
			int rightNum	= 0; 	// 구구단 입력 오른편 변수 ex) 7 
			int resultNum   = 0;    // 결과값 ex) 21 

			
			// LF1.   
			for(firstNum=2;firstNum<=lvl;firstNum++) {
								// LF2
				for(rightNum=1;rightNum<=lvl;rightNum++) {
					resultNum =    firstNum * rightNum;
					
					oneResult = super.viewDualNum(firstNum, rightNum, " X ", Integer.toString(resultNum));				
					finalResult += oneResult + "\n";  					
					
				}
				// EOF LF2  
			}
			
			
			
			// =========== 구구단 로직 종료 ===============
		} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값은 공백 
			finalResult = "";  
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return finalResult; 

    }
    // EOF doGuguDan
     
     /*
      *   2 X 2 ------> 4 
      *   2 X 3 ------> 6 
      */
}
// EOF Class 