package swedu.exercise;
 
 
/**
 * ID : Hong-1002-EXEC
 * 기능 : 구구단 실행 
 * @author 김지수 
 *
 */
public class CalcGuguExec {
 
	public static void main(String[] args) throws Exception  {
		 
		System.out.println( "\n\n 100  ============ 구구단 결과보기  ==============" );
		
		CalcNum_1hongkildong CNC = new CalcNum_1hongkildong(); 
		
		int guguLevel = 9;
		
		String resultOutString = ""; // 문자열 결과값 
		
		resultOutString = CNC.doGuguDan(guguLevel);
		
		System.out.println( "\n\n 100  ============ 구구단 결과보기  ==============" + guguLevel + "단");
		
		System.out.println( "구구단 결과값=" + resultOutString );
		
		System.out.println( "\n\n Team4  ============ 4팀 구구단 결과보기  ==============" + guguLevel + "단");
		
		CalcNum_4joseungyu CNC4 = new CalcNum_4joseungyu(); 
		
		resultOutString = CNC4.doGuguDan(guguLevel);
		
		System.out.println( "4팀 구구단 결과값\n=" + resultOutString );
		
		System.out.println( "\n\n Team3  ============ 3팀 구구단 결과보기  ==============" + guguLevel + "단");
		
		CalcNum_3doori CNC3 = new CalcNum_3doori(); 
		
		// resultOutString = CNC3.doGuguDan(guguLevel);
		
		//System.out.println( "3팀 구구단 결과값=\n" + resultOutString );
		


		
		System.out.println( "\n\n Team2  ============ 2팀 구구단 결과보기  ==============" + guguLevel + "단");
		
		CalcNum_2choieunjung CNC2 = new CalcNum_2choieunjung(); 
		
		//resultOutString = CNC2.doGuguDan(guguLevel);
		
		//System.out.println( "2팀 구구단 결과값=\n" + resultOutString );
		
		 
		System.out.println( "\n\n Team1  ============ 1팀 구구단 결과보기  ==============" + guguLevel + "단");
		
		CalcNum_1_kimyoungnam CNC1 = new CalcNum_1_kimyoungnam(); 
		
		//resultOutString = CNC1.doGuguDan(guguLevel);
		
		//System.out.println( "1팀 구구단 결과값=\n" + resultOutString );
		
	}
	// EOF main 
}
// EOF Class 