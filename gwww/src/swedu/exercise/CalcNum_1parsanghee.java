package swedu.exercise;

import javax.swing.text.View;

/**
 * ID : Hong-1010
 * 기능 : 다양한 숫자연산 자식 
 * @author 홍길동(cwjung123@gmail.com) 
 *
 */
public class CalcNum_1parsanghee extends CalcNumNew {
 
	/**
	 * 기본생성자 
	 */
    public CalcNum_1parsanghee() {
    	
    }
     
    /**
     * 구구단 반환 
     * @param lvl : 구구단 레벨 ( 9 or 19 ) 
     * @return
     * @throws Exception
     */
    public String doGuguDan(int lvl) throws Exception  {

    	// 계산된 결과를 반환하는 변수 
    	String finalResult = null;
    	String oneResult   = null; 
    
    		
    	CalcNumNew CN = new CalcNumNew();
		try 
		{
			//구구단 로직 시작
			for(int i = 1; i <= lvl; i ++)
			{
				for(int j = 1; j <= lvl; j ++)
				{
					//하나의 결과를 String으로 형변환
					oneResult = Integer.toString(i * j) ;
					
					//결과를  모아서 줄바꿈 
				    finalResult += CN.viewDualNum(i,j," X ", oneResult)+"\n";
					
						
						
					}
				}
				
			
			
			
			// =========== 구구단 로직 종료 ===============
			} 
		catch (Exception ex) {
			// 오류 발생시는 오류로그를 출력하고 결과값은 공백 
			finalResult = "";  
			ex.printStackTrace();
		} 
		
		// 출력값 전달 
		return finalResult; 

    }
    // EOF doGuguDan
     
     /*
      *   2 X 2 ------> 4 
      *   2 X 3 ------> 6 
      */
}
// EOF Class 