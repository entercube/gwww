package swedu.example;

// 생성자 오버로딩 
public class Test11_1 {

	/**
	 * @param args
	 */
	public Test11_1() {
		System.out.println("생성자1");
	}
	
	public Test11_1(int num) {
		System.out.println("생성자2");
		System.out.println(num);
	}
	
	public Test11_1(int num1, int num2) {
		System.out.println("생성자3");
		System.out.println(num1);
		System.out.println(num2);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test11_1 test1 = new Test11_1();
		Test11_1 test2 = new Test11_1(1);
		Test11_1 test3 = new Test11_1(1,2);

	}

}
