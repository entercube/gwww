package swedu.example;

/**
 * 영역을 구하는 클래스 
 * @author cwjung
 *
 */
class AreaN {
	
	int width,  length;
	
	public AreaN() {
		
	}
	
	/**
	 * 면적을 구하는 메서드 
	 * @param paramWidth : 가로 
	 * @param paramLength : 세로 
	 * @return
	 */
	public int getArea(int paramWidth, int paramLength) {
		
		int outValue = 0; // 계산값 결과 전달 변수 
		
		width 	= paramWidth;
		length 	= paramLength;
		
		// 사각형의 면적은 가로와 세로의 곱의 정의됨. 
		outValue = width * length;
		
		return outValue; 
	}
}


/**
 * 면적을 구하는 클래스 
 * @author cwjung
 *
 */
class VolumeN extends AreaN {
	
	int height;
	
	public VolumeN() {
		
	}
	
	public int getVolume(int paramWidth, int paramLength, int paramHeight) {
		
		int outValue = 0; // 계산값 결과 전달 변수 
		
		width 	= paramWidth;
		length 	= paramLength;
		height 	= paramHeight;
		
		// 다각형의 부피는 면적에 높이를 곱한 값으로 정의됨. 
		
		outValue = getArea(paramWidth, paramLength) * paramHeight;
		
		return outValue;
		
	}
}

public class Test13_1N {

	/**
	 * 상속을 이용한 육면체의 부피를 구하는 클래스 
	 * @param args
	 */
	public static void main(String[] args) {
		
		int outValue = 0; // 계산값 결과 전달 변수 
		
		// S01. 정육면체의 부피 계산 변수 
		int vwidth 		= 10; 
		int vlength 	= 20; 
		int vheight 	= 30; 
		
		// S10. 부피 구하는 객체 호출 
		VolumeN vmObj = new VolumeN();
		
		// S20. 정육면체의 부피값 확인 
		outValue = vmObj.getVolume(vwidth, vlength, vheight);
		 
		System.out.println("=========== 결과값 ====== >>> " + outValue  );
		
		
	}

}
