package swedu.example;


interface Engine {
	void printEngine();
}

interface Trans {
	void printTrans();
}


public class Test15_1 implements Engine, Trans {

	/**
	 * 인터페이스 
	 * @param args
	 */
	public void printEngine() {
		System.out.println("자동차 엔진");
	}
	
	public void printTrans() {
		System.out.println("자동차 변속기");
	}
	
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		Test15_1 car = new Test15_1();
		
		car.printEngine();
		car.printTrans();

	}

}
