package swedu.example;


public class Test4_1 {

	/**
	 * @param args
	 * 논리And 연산자가 논리Or 연산자보다 우선순위가 높다.
	 * 따라서, 아래의 아래의 프로그램은 오른쪽 && 연산자부터 처리되므로 출력결과는 true 이다.
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean result;
		result = 5 > 3 || 6 < 5 && 7 > 8;
		System.out.println(result);
		

	}

}
