
package swedu.example;

public class Test5_1 {

	/**
	 * @param args
	 *  >> 연산자는 최좌측 비트가 1인 경우 계속 1로 유지되나 >>> 연산자는 0으로 채워짐
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = -10;
		System.out.println(num>>1);  //결과 : -5
		System.out.println(num>>>1); //결과 : 2147483643

	}

}
