package swedu.example;
 
public class Test6_1 {

	/**
	 * 조건분기문 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = -7;
		
		if (num > 0) {
			System.out.println("양수");
		} else if (num < 0) {
			System.out.println("음수");
		} else {
			System.out.println("제로");
		}

	}

}
