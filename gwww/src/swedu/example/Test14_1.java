package swedu.example;

abstract class Shape {
	abstract void draw();
}

class Rectangle extends Shape {
	public void draw() {
		System.out.println("사각형 그리기");
	}
}

class Circle extends Shape {
	public void draw() {
		System.out.println("원 그리기");
	}
}

public class Test14_1 {

	/**
	 * 추상클래스와 다형성 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Rectangle rect = new Rectangle();
		rect.draw();
		Circle circle = new Circle();
		circle.draw();

	}

}
