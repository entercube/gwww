package swedu.example;


public class Test12_1 {

	/**
	 * 정적메서드 
	 * @param args
	 */
	public static int sumData(int num) {
		int i, sum = 0;
		for (i = 1; i <= num; i++) {
			sum += i;
		}
		return sum;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(Test12_1.sumData(10));

	}

}
