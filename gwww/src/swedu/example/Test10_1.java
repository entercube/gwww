package swedu.example; 
 
public class Test10_1 {

	/**
	 * 메서드 정의 
	 * @param args
	 */
	int sumData(int num) {
		int i, sum = 0;
		for (i = 1; i <= num; i++) {
			sum += i;
		}
		return sum;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test10_1 test = new Test10_1();
		System.out.println(test.sumData(10));

	}

}
