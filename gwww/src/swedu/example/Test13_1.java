package swedu.example;

class Area {
	int width, length;
	public Area() {
	}
	public int getArea(int w, int l) {
		width = w;
		length = l;
		return width * length;
	}
}


class Volume extends Area {
	
	int height;
	
	public Volume() {
	}
	
	public int getVolume(int w, int l, int h) {
		width = w;
		length = l;
		height = h;
		return getArea(w, l) * height;
	}
}

public class Test13_1 {

	/** 
	 * 상속과 접근지정 
	 * @param args
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Volume v = new Volume();
		System.out.println(v.getVolume(10, 20, 30));
		
	}

}
