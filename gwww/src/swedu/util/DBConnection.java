package swedu.util; 

import java.sql.Connection;
import java.sql.DriverManager;
 
import javax.naming.*;
import javax.sql.DataSource;

 
/**
 * ID : SWEDU-UTIL-2300-JU
 * 기능 : 데이터베이스 접속 
 * @version : 0.9  
 * @author : cwjung ( nightfog777@gmail.com  ) 
*/ 

public class DBConnection {
  
	// 1. 로컬 DB접속 
	public static final int LOCAL_CONNECT 			= 1;			// 로컬 DB 접속 --> Java Application 
 	
	// 2. 풀링 DB접속 
	public static final int POOL_CONNECT 			= 2;			// 서블릿 컨테이너 DB 접속 --> Web Application 	
	
	// 3. 데이터 이동 등 
	public static final int SOURCE_CONNECT 			= 3;			// 소스 DB 접속 
	public static final int TARGET_CONNECT 			= 4;			// 목표 DB 접속 

	// 4. 테스트 접속 
	public static final int TEST_CONNECT 			= 5;			// 테스트 DB 접속 

	
	// 접속 컨텍스트 및 데이터 소스 
	private static Context initCtx 	= null; 
	private static Context envCtx 	= null; 
	private static DataSource ds 	= null;
	
	/*
	 * 기본생성자 
	 */
	public DBConnection() throws Exception {
		
	} 
	 
	/**
	 * 로컬데이터 접속 
	 * @return
	 * @throws Exception
	 */
	public static Connection getLocalConnection() throws Exception{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		String dbsrc = "DB for internet Education"; 
		String dburl = "jdbc:mysql://mydb.nuriblock.com:3306/covidb";   // "jdbc:mysql://db.ibaeum.com/swbasedb";
		String dbusr = "devdba";
		String dbpwd = "devdba12!";  
  ;  
  
		// System.out.println( "EXP-DB130. Connection  getTargetConnection : " + dbsrc );
		
		return DriverManager.getConnection(dburl + "?useSSL=false&useUnicode=true&characterEncoding=utf8", dbusr, dbpwd);	
 		
	}
	
	
	/**
	 * 소스데이터 접속 
	 * @return
	 * @throws Exception
	 */
	public static Connection getSourceConnection() throws Exception{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		String dbsrc = "DB for internet Education"; 
		String dburl = "jdbc:mysql://mydb.nuriblock.com:3306/covidb";   // "jdbc:mysql://db.ibaeum.com/swbasedb";
		String dbusr = "devdba";
		String dbpwd = "devdba12!";  
  
  
		// System.out.println( "EXP-DB130. Connection  getTargetConnection : " + dbsrc );
		
		return DriverManager.getConnection(dburl + "?useSSL=false&useUnicode=true&characterEncoding=utf8", dbusr, dbpwd);	
 		
	}
	

	/**
	 * 목표 데이터 접속 
	 * @return
	 * @throws Exception
	 */
	public static Connection getTargetConnection() throws Exception{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		String dbsrc = "DB for internet Education"; 
		String dburl = "jdbc:mysql://mydb.nuriblock.com:3306/covidb";   // "jdbc:mysql://db.ibaeum.com/swbasedb";
		String dbusr = "devdba";
		String dbpwd = "devdba12!";  
  
		// System.out.println( "EXP-DB130. Connection  getTargetConnection : " + dbsrc );
		
		return DriverManager.getConnection(dburl + "?useSSL=false&useUnicode=true&characterEncoding=utf8", dbusr, dbpwd);	
 		
	}
 
	 
	/**
	 * 테스트 데이터 접속 
	 * @return
	 * @throws Exception
	 */
	public static Connection getTestConnection() throws Exception{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		String dbsrc = "DB for S/W Education"; 
		String dburl = "jdbc:mysql://mydb.nuriblock.com:3306/covidb";   // "jdbc:mysql://db.ibaeum.com/swbasedb";
		String dbusr = "devdba";
		String dbpwd = "devdba12!";  
  
		// System.out.println( "EXP-DB140. Connection ibaeum for getTestConnection : " + dbsrc );
		
		return DriverManager.getConnection(dburl + "?useSSL=false&useUnicode=true&characterEncoding=utf8", dbusr, dbpwd);	
 		
	}
 

	/**
	 * 컨테이너 풀로부터 DB접속 
	 * @throws Exception
	 */
	public static Connection getDBContainer() throws Exception { 
		 
		initCtx = new InitialContext(); 
		
		envCtx = (Context)initCtx.lookup("java:comp/env"); 
		
		ds = (DataSource)envCtx.lookup("jdbc/eqnight");

		// System.out.println( "EXP-DB200. Connection getDBContainer");
		return ds.getConnection();
		
	}
 
}