package swedu.util; 

import java.io.*;


/**
 * ID : SWEDU-UTIL-2020-JU
 * 기능 : 파일처리 유틸리티 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class Managefile {
	
    //버퍼처리상수
    final static int NORMAL = 0;
    final static int REVERSE = 1;

    public Managefile() {
    }

    /**
	 * 파일내용얻어옴
	 * @param fname
	 * @param buf
	 * @param mode
	 */
    public void getFile(String fname,StringBuffer buf,int mode) {
        String str = null;
        buf.setLength(0);               //버퍼내용을 비운다.
        try {
            FileReader fr = new FileReader(fname);
            BufferedReader br = new BufferedReader(fr);
            while ((str = br.readLine()) != null) 	{
                 buf.append(str + "\n");
            }
            fr.close();
            br.close();

        }catch (Exception e) {e.printStackTrace();}

        if ( mode == NORMAL ) {
        }
        else if ( mode == REVERSE ) {
            buf = buf.reverse();
        }
        else {
        }

    }
    //getFile


    /**
     * 파일의 내용을 문자열변수로 옮김 
     * @param fname
     * @return
     */
    public String getFile(String fname) {
        String str = "";
        String outstr = "";
        int i =0;
        try {
            FileReader fr = new FileReader(fname);
            BufferedReader br = new BufferedReader(fr);

            while ((str = br.readLine()) != null) 	{
                 str = (i==0) ? str : "\n" + str;
                 outstr = outstr + str;
                 i++;
            }
            fr.close();
            br.close();

        }
        catch (Exception e) {e.printStackTrace();}
        return outstr;
    }
    //getFile - to string

    
    /**
     * 파일라인세기 
     * @param fname
     * @return
     */
    public int getFileCnt(String fname) {
    	
    	int fcnt = 0;
    	
 
        try {
            FileReader fr = new FileReader(fname);
            BufferedReader br = new BufferedReader(fr);

            while (( br.readLine() ) != null) 	{
                fcnt++; 
            }
            
            fr.close();
            br.close();

        }catch (Exception e) {e.printStackTrace();}
        
        return fcnt;
        
    } 

    /**
	 * 파일에서 한줄만 얻어옴
	 * @param fname
	 * @return String
	 */
    public String getLine(String fname) {
        String str = "";
        try {
            FileReader fr = new FileReader(fname);
            BufferedReader br = new BufferedReader(fr);

            if ((str = br.readLine()) != null) 	{

            }
            fr.close();
            br.close();

        }catch (Exception e) {e.printStackTrace();}
        return str;
    }
    //getLine - to string  1줄만 파일에서 읽음


    /**
	 * 파일에서 특정줄 읽음
	 * @param fname
	 * @param lineno
	 * @return String
	 */
    public String getLine(String fname,int lineno) {
        String str = "";
        int i =0;
        try {
            FileReader fr = new FileReader(fname);
            BufferedReader br = new BufferedReader(fr);

            while ((str = br.readLine()) != null ){
                i++;
                if ( i == lineno ) break;
            }
            fr.close();
            br.close();
        }catch (Exception e) {
            System.out.println("File Check!");
			//e.printStackTrace();
		}
        return str;
    }
    //getLine - to string  1줄만 파일에서 읽음


    public void display(StringBuffer buf) {
         System.out.println(buf.toString());
    } 
    // end display

    
    
    /**
     * 문자버퍼 파일에 덧대어 쓰기 
     * @param inBuffer
     * @param outFname
     * @throws Exception
     */
    public void  writeAppend(StringBuffer inBuffer, String outFname) throws Exception {
 
    	FileWriter fw = new FileWriter(outFname,true);
        
    	fw.write(inBuffer.toString());
        
    	fw.close();
    } 
    // End writeAppend




    /**
     * 문자버퍼 파일에 덧대어 쓰기 
     * @param inBuffer
     * @param outFname
     * @throws Exception
     */
    public void  writeBufAppendUtf8(StringBuffer inBuffer, String outFname) throws Exception {
 
        Writer out = new BufferedWriter(new OutputStreamWriter(  new FileOutputStream(outFname), "UTF-8" ));
        
        try {
        	out.append(inBuffer.toString());
        } 
        catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	out.close();
        } 
    } 
    // End writeBufAppendUtf8

    
    
    /**
     * 문자열 덧붙여 쓰기 기본 인코등 ( 1522 ) 
     * @param inStr
     * @param outFname
     * @throws Exception
     */
    public void writeAppend(String inStr, String outFname) throws Exception {
    	// File f = new File(fname);
    	
		// System.out.println("str=" + inStr + " fname=" + fname);
		
        FileWriter fw = new FileWriter(outFname, true);
        fw.write(inStr);
        fw.close();
    } 
    // End writeAppend


    /**
     * UTF8로 파일에 문자열 덧붙이기 
     * @param inStr
     * @param outFname
     * @throws Exception
     */
    public void writeStrAppendUtf8(String inStr, String outFname) throws Exception {
 
        Writer out = new BufferedWriter(new OutputStreamWriter(  new FileOutputStream(outFname), "UTF-8" ));
  
        try {
        	out.append(inStr);
        } 
        catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	out.close();
        } 
 
    } 
    // End writeStrAppendUtf8


    
    
    
    /**
	 * 한번에 파일 쓰기 
     * buffer 입력스트링 버퍼 
	 * @param buf
	 * @param fname
	 * @throws Exception
	 */
    public void writeOnce(StringBuffer inBuffer, String outFname) throws Exception {
        
    	File outf=new File(outFname);
        if (outf.exists()) { outf.delete(); }

        FileWriter fw = new FileWriter(outf);
        fw.write(inBuffer.toString());
        fw.close();

    } 
    // End writeOnce

    



    /**
	 * writeOnce
     * buffer 입력스트링 버퍼 
	 * @param buf
	 * @param fname
	 * @throws Exception
	 */
    public void writeBufUtf8(StringBuffer inBuffer,String outFname) throws Exception {
        
    	File outf=new File(outFname);
    	
        if (outf.exists()) { outf.delete(); }
 
        Writer out = new BufferedWriter(new OutputStreamWriter(  new FileOutputStream(outFname), "UTF-8" ));
        
        try {
        	out.write(inBuffer.toString() );
        } 
        catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	out.close();
        }  
        
 
    } 
    // End writeBufUtf8

    
    
    /**
     * 스트링버퍼를 파일로 저장 
     * @param inBuffer
     * @param outFname
     * @param outEncoding
     * @throws Exception
     */
    public void writeBufEncoging(StringBuffer inBuffer,String outFname, String outEncoding) throws Exception {
        
    	File outf=new File(outFname);
    	
        if (outf.exists()) { outf.delete(); }
 
        Writer out = new BufferedWriter(new OutputStreamWriter(  new FileOutputStream(outFname), outEncoding ));
        
        try {
        	out.write( inBuffer.toString() );
        } 
        catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	out.close();
        }  
        
 
    } 
    // End writeBufEncoging

    
    
    

    /**
     * 한번에 파일로 저장 : 기본인코딩  ( 1252  ) 
     * @param inStr : 입력문자열 
     * @param outFname : 저장파일명 
     * @throws Exception
     */
    public void writeOnce(String inStr,String outFname) throws Exception {
    	
        File outf=new File(outFname);

        if (outf.exists()) { outf.delete(); }

        FileWriter fw = new FileWriter(outf);
        
        fw.write(inStr);
        
        fw.close();
        
    } 
    // End writeOnce

    
    
    /**
     * 한번에 EUC-KR인코딩으로 쓰기 
     * @param inStr : 입력문자열 
     * @param outFname : 저장파일명 
     * @throws Exception
     */
    public void writeOnceKr(String inStr,String outFname) throws Exception {
    	
        File outf=new File(outFname);

        if (outf.exists()) { outf.delete(); }

        Writer out = new BufferedWriter(new OutputStreamWriter(  new FileOutputStream(outFname), "EUC-KR" ));
        
        try {
        	out.write(inStr);
        } 
        catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	out.close();
        }  
        
    } 
    // End writeOnceKr

 
    
    /**
     * 한번에 UTF-8인코딩으로 쓰기 
     * @param inStr : 입력문자열 
     * @param outFname : 저장파일명 
     * @throws Exception
     */
    public void writeStringUtf8(String inStr, String outFname) throws Exception {
       
    	File outf=new File(outFname);

        if (outf.exists()) { outf.delete(); }

        Writer out = new BufferedWriter(new OutputStreamWriter(  new FileOutputStream(outFname), "UTF-8" ));
        
        try {
        	out.write(inStr);
        } 
        catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	out.close();
        }  
        
    } // End writeOnceUtf8

    



    /**
     * 한번에 특정 인코딩으로 쓰기 
     * @param  inStr : 입력문자열 
     * @param outFname : 저장파일명 
     * @param outEncoding : 저장인코딩 
     * @throws Exception
     */
    public void writeStringEncoging(String inStr, String outFname, String outEncoding ) throws Exception {
       
    	File outf = new File(outFname);

        if (outf.exists()) { outf.delete(); }

        Writer out = new BufferedWriter(new OutputStreamWriter(  new FileOutputStream(outFname), outEncoding ));
        
        try {
        	out.write(inStr);
        } 
        catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	out.close();
        }  
        
    } 
    // End writeStringEncoging

    
    
    
    
    /**
	 * 줄단위 쓰기
     * 줄단위로 문자열가공시 사용
	 * @param buf
	 * @param fname
	 * @throws Exception
	 */
    public void writeLine(StringBuffer buf,String fname) throws Exception {

        String tStr = null;
        File outf=new File(fname);

        if (outf.exists()) { outf.delete(); }

        StringReader sinpt = new StringReader(buf.toString());
        FileWriter fw = new FileWriter(outf);
        BufferedReader br = new BufferedReader(sinpt);

        while ((tStr = br.readLine()) != null) {
              fw.write(tStr + "\n");
        }
        
        fw.close();
        br.close();
        sinpt.close();

    }
    //end writeLine

    
    /**
	 *  줄단위 쓰기
     *  줄단위로 문자열가공시 사용
	 * @param tstr
	 * @param fname
	 * @throws Exception
	 */
    public void writeLine(String tstr,String fname) throws Exception {

        String tStr = null;
        File outf=new File(fname);

        if (outf.exists()) { outf.delete(); }

        StringReader sinpt = new StringReader(tstr);
        FileWriter fw = new FileWriter(outf);
        BufferedReader br = new BufferedReader(sinpt);

        while ((tStr = br.readLine()) != null) {
              fw.write(tStr + "\n");
        }
        fw.close();
        br.close();
        sinpt.close();
    }
    //end writeLine


    /**
	 * 입력파일을 출력파일로 지정된 줄수만큼 저장
	 * @param infile
	 * @param outfile
	 * @param linecnt
 	 * @param upd
	 * @throws Exception
	 */
    public void writeLine(String infile,String outfile,int linecnt,boolean upd) throws Exception {

        String tStr = null;
        
        File outf=new File(outfile);

        if (outf.exists()) { outf.delete(); }

        FileReader fr = new FileReader(infile);
        BufferedReader br = new BufferedReader(fr);
        FileWriter fw = new FileWriter(outf);

        int i =0;
        while ((tStr = br.readLine()) != null) 	{
			  if ( upd && i == linecnt ) break; // 라인수만큼만 진행
              fw.write(tStr + "\n");
              i++;
        }
        
        fr.close();
        br.close();
        fw.close();
    }
    //end writeLine


    
    
    
    /**
	 * 입력파일내의 모든문자 줄바꿈 문자 넣어서 저장 
	 * @param infile
	 * @param outfile
	 * @param linecnt
 	 * @param upd
	 * @throws Exception
	 */
    public void writeCharLine(String infile,String outfile ) throws Exception {

        int 	tChar = 0;
 
        
        File outf=new File(outfile);

        if (outf.exists()) { outf.delete(); }

        FileReader fr = new FileReader(infile);
        BufferedReader br = new BufferedReader(fr);
        FileWriter fw = new FileWriter(outf);
        
   
        while ( (tChar = br.read()) > -1 ) 	{
        	
        	//System.out.println(  tChar  + "  to  " + (char)tChar );
        	if ( tChar > 13000  && tChar < 44032 ) {  // 44032 한글 가 
        		fw.write( (char)tChar + "," + tChar + "\n");
        	}
 
        
        }
        
        fr.close();
        
        br.close();
        
        fw.close();
        
    }	
    // End writeCharLine     
    
    /**
  	 * 입력파일내의 모든문자 줄바꿈 문자 넣어서 저장 
  	 * @param infile
  	 * @param outfile
  	 * @param linecnt
   	 * @param upd
  	 * @throws Exception
  	 */
      public void writeCharLine(String infile,String outfile, String delim ) throws Exception {

          int 	tChar = 0;
   
          
          File outf=new File(outfile);

          if (outf.exists()) { outf.delete(); }

          FileReader fr = new FileReader(infile);
          BufferedReader br = new BufferedReader(fr);
          FileWriter fw = new FileWriter(outf);
          
        
          while ( (tChar = br.read()) > -1 ) 	{
          	
        	  fw.write( (char)tChar + delim + tChar + "\n");
 
          }
          
          fr.close();
          
          br.close();
          
          fw.close();
          
      }	
      // End writeCharLine     
      
    
    /**
     * 파일을 바이트로 변환 
     * @param file
     * @return
     * @throws IOException
     */
    public static byte[] file2Byte(String file) throws IOException {
        InputStream in = null;
        byte[] pix = null;
        try {
          in = new FileInputStream(file);
          ByteArrayOutputStream baos = new ByteArrayOutputStream();
          byte[] b = new byte[1024];
          int j;
          while ((j = in.read(b)) != -1) {
            baos.write(b, 0, j);
          }
          pix = baos.toByteArray();
        } finally {
          if (in != null) {
            try {
              in.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
        return pix;
      }
    
    /**
     * 바이트를 파일로 벼환 
     * @param b
     * @param outputFile
     * @throws IOException
     */
      public static void byte2File(byte[] b, String outputFile)
          throws IOException {
        BufferedOutputStream stream = null;
        try {
          File file = new File(outputFile);
          FileOutputStream fstream = new FileOutputStream(file);
          stream = new BufferedOutputStream(fstream);
          stream.write(b);
        } finally {
          if (stream != null) {
            try {
              stream.close();
            } catch (IOException e1) {
              e1.printStackTrace();
            }
          }
        }
      }
      // end byte2File 

 
         
        

    /**
     * 파일복수 
     * @param inFileName : 입력파일명 
     * @param outFileName : 출력파일명 
     */
	public void fileCopy(String inFileName, String outFileName) {
		try {
			FileInputStream fis = new FileInputStream(inFileName);
			FileOutputStream fos = new FileOutputStream(outFileName);
    	   
			int data = 0;
			while((data=fis.read())!=-1) {
				fos.write(data);
			}
			fis.close();
			fos.close();
    	   
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
      
	/**
	 * 파일삭제       
	 * @param inFileName : 삭제대상파일 
	 * @return 파일삭제결과여부 
	 */
	public boolean fileDelete(String inFileName) {
		
		boolean delUPD = false;
		
		File f = new File(inFileName);
 
		if( f.exists() )  {
			
			delUPD = f.delete();
	     
		}
		
		return delUPD;
		
	}
      
	
	/**
	 * 스트링버퍼 파일저장 
	 * @param br : 스트링버퍼 
	 * @param outFile : 저장파일 
	 * @return
	 * @throws Exception
	 */
	public int bufferToFile (BufferedReader br, String outFile ) throws Exception  {
		 
		String inputLine;
		File file = new File(outFile);

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);

		while ((inputLine = br.readLine()) != null) {
			bw.write(inputLine +  "\r\n" ) ;  // 도스타입 저장  \r\n   linux \n max \r  
		}

		bw.close();
		br.close();
		
		if ( file.exists() ) {
			return 1;
		}
		else {
			return 0;
		}
	}
	
	/**
	 * 스트링버퍼 파일저장 
	 * @param br : 스트링버퍼 
	 * @param outFile : 저장파일 
	 * @param EncodeType : 파일 저장 엔코딩 타입 ( 예 : UTF-8) 
	 * @param bomUPD : BOM (byte order mark) 추가여부 ( utf 8일 경우 ) 
	 * @return
	 * @throws Exception
	 */
	public int bufferToFile (BufferedReader br, String outFile, String EncodeType, boolean bomUPD ) throws Exception  {
		 
		String inputLine;

		OutputStreamWriter  OST = new OutputStreamWriter(  new FileOutputStream(outFile), EncodeType );
		
		//FileWriter fw = new FileWriter(file.getAbsoluteFile());
		//BufferedWriter bw = new BufferedWriter(fw);
		
		BufferedWriter bw = new BufferedWriter( OST );
		
		
		int i = 0;
		while ((inputLine = br.readLine()) != null) {
			if ( bomUPD && i == 0 ) {
				bw.write("\uFEFF");
			}
			i++;
			bw.write(inputLine + "\r\n" ) ;  // 도스타입 저장  \r\n   linux \n max \r  
		}

		bw.close();
		br.close();
		
		// 파일 저장 
		File file = new File(outFile);
		 
		if ( file.exists() ) {
			return 1;
		}
		else {
			return 0;
		}
		
	}
 
}    
// End of Class 
