package swedu.util; 

import java.io.*;
import java.util.ArrayList;
import java.util.List;
 

/**
 * ID : SWEDU-UTIL-2000-JU
 * 기능 : Java 소프트웨어 설정 및 구동 확인
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class DirUtil {
	
    final static int NORMAL 		= 0; 		// 정상모드 
    final static int RECURSIVE 		= 1;		// 재귀모드 

    List<String>	flist	= new ArrayList<String>();
    
    public DirUtil() {
    }

    /**
     * 디렉토리내 화일 및 디렉토리 명 출력  
     * @param dirSrc
     * @param mode
     * @throws Exception
     */
    public void showDirFile(String dirSrc, int mode )   throws Exception {  
		File dir = new File(dirSrc); 
		File[] fileList = dir.listFiles(); 
	 
		for(int i = 0 ; i < fileList.length ; i++){
			
			File file = fileList[i]; 
			
			if(file.isFile()){
				System.out.println("FileName=" + file.getAbsolutePath().toString());
			}
			else if(file.isDirectory()){
				System.out.println("DirName=" + file.getAbsolutePath().toString());
				if ( mode == RECURSIVE ) {
					showDirFile(file.getCanonicalPath().toString(), mode ); 
				}
			}
			
		} // End for
 
	} // End of showDirFile


    /**
     * 디렉토리내 화일 및 디렉토리 명 출력  
     * @param dirSrc
     * @param mode
     * @throws Exception
     */
    
    
    /**
     * 디렉토리내 파일 출력 
     * @param dirSrc
     * @param extPattern : 확장자 패턴 
     * @param mode  : RECURSIVE (1) 일 경우 재귀호출 
     * @throws Exception
     */
    public void showDirFile(String dirSrc, String extPattern, int mode )   throws Exception {  
		File dir = new File(dirSrc); 
		File[] fileList = dir.listFiles(); 
		
		String ext = null;
		String fname = null;
		
		for(int i = 0 ; i < fileList.length ; i++){
			
			File file = fileList[i]; 
			
			if(file.isFile()){
				fname =  file.getName();   // fname = fbody + "." + ext ( abcd.jpg = abcd + "." + jpg )
				ext = fname.substring( fname.indexOf(".") + 1 ).toLowerCase();  // 확장자 소문자출력 
				if ( ext.matches(extPattern)) {
					System.out.println("Matched FileName=" + file.getAbsolutePath().toString());
				}
				else {
					System.out.println("Unmatched FileName=" + file.getAbsolutePath().toString());
				}
			}
			else if(file.isDirectory()){
				System.out.println("DirName=" + file.getAbsolutePath().toString());
				if ( mode == RECURSIVE ) {
					showDirFile(file.getCanonicalPath().toString(), extPattern, mode ); 
				}
			}
			
		} // End for
 
	} // End of showDirFile for pattern 

    

    /**
     * 디렉토리내 화일 및 디렉토리 명 출력  
     * @param dirSrc
     * @param mode
     * @throws Exception
     */
    public List<String> getDirFile(String dirSrc, int mode )   throws Exception {  
		
    	File dir = new File(dirSrc); 
		File[] fileList = dir.listFiles(); 
		String fnameAll = null;
		for(int i = 0 ; i < fileList.length ; i++){
			
			File file = fileList[i]; 
			
			if(file.isFile()){
				fnameAll = file.getAbsolutePath().toString();
				flist.add(fnameAll);
				//System.out.println("FileName=" + file.getAbsolutePath().toString());
			}
			else if(file.isDirectory()){
				//System.out.println("DirName=" + file.getAbsolutePath().toString());
				if ( mode == RECURSIVE ) {
					getDirFile(file.getAbsolutePath().toString(), mode ); 
				}
			}
			
		} // End for
		
		return flist; 
 
	} // End of getDirFile
    
    


    /**
     * 디렉토리내 화일 가져오기 
     * @param dirSrc
     * @param extPattern : 확장자 패턴 
     * @param mode
     * @throws Exception
     */
    public List<String>  getDirFile(String dirSrc, String extPattern, int mode )   throws Exception {  
 
    	File dir = new File(dirSrc); 
		
    	File[] fileList = dir.listFiles(); 
		
		String ext 			= null;		// 확장자
		String fname 		= null;		// 파일명 
		String fnameAll 	= null;  	// 절대경로명
		
		for(int i = 0 ; i < fileList.length ; i++){
			
			File file = fileList[i]; 
			
			if(file.isFile()){
				fname =  file.getName();   // fname = fbody + "." + ext ( abcd.jpg = abcd + "." + jpg )
				ext = fname.substring( fname.indexOf(".") + 1 ).toLowerCase();  // 확장자 소문자출력 
				fnameAll = file.getAbsolutePath().toString();
				if ( ext.matches(extPattern)) {
					flist.add(fnameAll);
					//System.out.println("Matched FileName=" + fnameAll);
				}
				else {
					//System.out.println("Unmatched FileName=" + fnameAll);
				}
			}
			else if(file.isDirectory()){
				//System.out.println("DirName=" + file.getAbsolutePath().toString());
				if ( mode == RECURSIVE ) {
					getDirFile(file.getAbsolutePath().toString(), extPattern, mode ); 
				}
			}
			
		} // End for
 
		return flist; 
		
	} 
    // End of getDirFile  for pattern 
  
	 

} 
// End of Class 
