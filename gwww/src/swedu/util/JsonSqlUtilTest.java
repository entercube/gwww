package swedu.util; 
 
 
import org.json.simple.JSONArray;
 
import swedu.util.NumUtil;
 

/**
 * ID : SWEDU-UTIL-2010-JT
 * 기능 : Json Sql 처리 유틸리티 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 


public class JsonSqlUtilTest {
 
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		// (공통)시간 측정 --------------------------------------------------------------------------------
		long stime =0, etime=0,  diff = 0;
		stime = System.currentTimeMillis();	
		 
		// 1. 암호화 
		System.out.println("============  1. JSON SQL String  ======================") ;
 
		JSONArray jArr = new JSONArray();
		
		Object numObj = null ;
		
		for( int i = 0; i < 10; i++) {
			numObj =  (Object)NumUtil.getRandomInt(1, 30);
			jArr.add( i, numObj );
		}
		
		System.out.println("T1 jArr To String  = " +  jArr.toJSONString() );

		String outStr = JsonSqlUtil.getJSQLStr(jArr, JsonSqlUtil.QUOTATION_NO);  
		
		System.out.println("T2 getJSQLNum = " +  outStr  );
		
		outStr = JsonSqlUtil.getJSQLStr(jArr, JsonSqlUtil.QUOTATION_YES);  
		
		System.out.println("T3 getJSQLNum = " +  outStr  );
		
 /*
		System.out.println("============  2. JSON SQL String using json Object   ======================") ;

		JSONArray jArr2  = new JSONArray();
		
		JSONObject jObj2 = null;
  
		for ( int i = 0; i < 10; i++) {
			jObj2 = new JSONObject() ;
			jObj2.put( i , NumUtil.getRandomInt(1, 30) );
 			jArr2.add( jObj2 );
		}
		
		System.out.println(" jArr To String  = " +  jArr2.toJSONString() );
		
		outStr = JsonUtil.getJSQLStr2(jArr2, JsonUtil.QUOTATION_NO);  
		
		System.out.println(" getJSQLNum2(jArr)  = " +  outStr  );
		
		outStr = JsonUtil.getJSQLStr2(jArr2, JsonUtil.QUOTATION_YES); 
		 
		System.out.println(" getJSQLNum2(jArr)  = " +  outStr  );
		
*/
		 
		// (공통) 시간측정 종료 =========================================== 
		etime = System.currentTimeMillis();
		
		diff = etime - stime;

		System.out.println("Time : " + diff + " ms" );
	    
	  }
 
} 
// End CLass 


