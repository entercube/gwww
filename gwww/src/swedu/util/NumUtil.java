package swedu.util; 

 
import java.util.*;

/**
 * ID : SWEDU-UTIL-2050-JU
 * 기능 : 숫자처리 유틸리티 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class NumUtil {

    public NumUtil() {
    }
    
    /**
     * 랜덤숫자만들기  (1만에서 5만까지의 숫자 임의 출력 ) 
     * @param start  ex) 10000
     * @param end    ex) 50000
     * @return
     */
	public static int getRandomInt(int start, int end) {
		Random randGen = new Random();	
 
 
        double range = end - start + 1;
        int out = (int)(randGen.nextDouble() * range + start);   
        	
		return out;
	}

	/**
	 * 숫자에 대한 배열 생성 
	 * @param array
	 */
	public static void shuffleArray(int[] array) {
	   
		int index, temp;
	    
	    Random random = new Random();
	    
	    for (int i = array.length - 1; i > 0; i--) {
	        index = random.nextInt(i + 1);
	        temp = array[index];
	        array[index] = array[i];
	        array[i] = temp;
	    }
	    
	}

	
	/**
	 * 정수리스트 배열 변환 
	 * @param integerList
	 * @return
	 */
	public static int[] toIntArray(List<Integer> integerList) {  
		
        int[] intArray = new int[integerList.size()];  
        
        for (int i = 0; i < integerList.size(); i++) {  
            intArray[i] = integerList.get(i);  
        }  
        
        return intArray;  
    }  
 
	
}
// End Class