package swedu.util; 
  
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
 
/**
 * ID : SWEDU-UTIL-2010-JU
 * 기능 : Json Sql 처리 유틸리티 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class JsonSqlUtil {

	public final static int QUOTATION_NO = 0;

	public final static int QUOTATION_YES = 1;
 
	/**
	 * JSON 문자열의 value가 숫자일 경우 DB에서 숫자처리를 위한 sql 문자열 작성  
	 * 연관배열활용 jArr To String  = [13,15,5,27,23,30,5,11,9,3]
	 * @param jArr
	 * @return  예 : ( 2, 5, 23, 45 ) 
	 * @throws Exception
	 */
	public static String getJSQLStr(JSONArray jArr, int quotationType ) throws Exception  {
		
		
		String tmpValue = null;

		String tStr 	= null;
	
		String ST		= null;		// 시작 
		String MD		= null;		// 중간
		String EN		= null;     // 최종
		
		if ( jArr.size() == 0 ) return "";

		
		if ( quotationType == QUOTATION_YES ) { // 인용부호 삽입 ( 문자열 SQL 처리 ) 
			
			ST = "('";
			MD = "', '";
			EN = "')";
		}
		else {
			ST = "(";
			MD = ", ";
			EN = ")";
		}
		 
	    int aCnt = jArr.size();
	    
	     
		for(int i=0; i < aCnt ; i++){
			 
			tmpValue 	=   jArr.get(i).toString();	 		// JSON객체의 키에 해당하는 값을 담음 
			
			 // System.out.println( tmpValue );
			
			if ( i == 0 )	tStr = ST + tmpValue; 
			else  tStr +=   MD + tmpValue;
		
		}
			
		tStr  +=   EN ;
  	
		return tStr;
		
	} // EOF getJsonSQLNum 
 

	/**
	 * JSON 문자열의 value가 숫자일 경우 DB에서 숫자처리를 위한 sql 문자열 작성  
	 * 색인배열활용 jArr To String  = [{"0":17},{"1":5},{"2":6},{"3":4},{"4":18},{"5":20},{"6":21},{"7":23},{"8":23},{"9":26}]
	 * @param jArr
	 * @return  예 : ( 2, 5, 23, 45 ) 
	 * @throws Exception
	 */
	public static String getJSQLStr2(JSONArray jArr, int quotationType  ) throws Exception  {
		
		String tmpValue = null;
		String tStr 	= null;
		
		if ( jArr.size() == 0 ) return "";
		
	 	JSONObject jo = new JSONObject();
 
		String ST		= null;		// 시작 
		String MD		= null;		// 중간
		String EN		= null;     // 최종
		
		if ( quotationType == QUOTATION_YES ) { // 인용부호 삽입 ( 문자열 SQL 처리 ) 
			ST = "('";
			MD = "', '";
			EN = "')";
		}
		else {
			ST = "(";
			MD = ", ";
			EN = ")";
		}

	    int aCnt = jArr.size();
 
		for(int kn = 0; kn < aCnt ; kn++){  // kn : keynumber 
			
			 jo = (JSONObject)jArr.get(kn);
 
			 tmpValue 	=   jo.get("value").toString();
			 
				if ( kn == 0 )	tStr = ST + tmpValue; 
				else  tStr +=   MD + tmpValue;
			
			}
				
			tStr  +=   EN ;
		
		return tStr;
		
	} 
	// EOF getJsonSQLNum2 
 
} 
// End CLass 


