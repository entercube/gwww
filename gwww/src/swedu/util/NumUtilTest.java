package swedu.util;

 
/**
 * ID : SWEDU-UTIL-2050-JT
 * 기능 : 숫자처리 유틸리티 테스트
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class NumUtilTest {
	
    public static void main(String[] args) throws Exception {
    	 
    	// (공통)시간 측정 --------------------------------------------------------------------------------
    	long stime =0, etime=0,  diff = 0;
    	stime = System.currentTimeMillis();	
    	 
    	int startNum = 1000; 
    	int endNum	 = 9999; 
		
    	// 시작정수와 종료정수 사이의 랜덤 숫자를 가져옴 
    	int randNum = NumUtil.getRandomInt(startNum, endNum); 
    	
		System.out.println(" ======== R100 randNum = " +  randNum );
		
		 
		// (공통) 시간측정 종료 =========================================== 
		etime = System.currentTimeMillis();
		
		diff = etime - stime;

		System.out.println("Time : " + diff + " ms" );
		
	} 

}
// End class

 