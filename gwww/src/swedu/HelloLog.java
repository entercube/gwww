package swedu;

import org.apache.log4j.Logger;
 
/**
 * ID : SWEDU-TEST-1010
 * 기능 : Java 로그동작 
 * @version : 0.9  
 * @author : sw.tutor 
*/ 

public class HelloLog {
	
	public static void main(String[] args) {
		// 문자열 출력 
	 
		Long  ctime  = System.currentTimeMillis(); 
		 
		//PropertyConfigurator.configure("log4j.properties");
		
		 
		Logger logger  = Logger.getLogger("T1010");

		System.out.println("================= Logger Test ==============="); 
		
		logger.info("L100 currentTime=" + ctime);
 
	}
	
}
 